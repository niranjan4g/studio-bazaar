-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2020 at 12:26 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eit_ecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`) VALUES
(1, 'Admin', 'staradmin', '$2y$10$TJBbvUHbwiQRA1hkBhcMd.YKDCyV9lrwNFg438cEx3kx0d2jgH3Su');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `catid` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_cover` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`catid`, `category_id`, `category_name`, `category_cover`, `created_by`, `created_on`, `updated_by`, `updated_on`, `is_active`) VALUES
(2, 'STBCAT886154', 'Wedding Photography', '301300328wed.jpg', 'Admin', '2020-05-29 05:08:59', '', '0000-00-00 00:00:00', 1),
(3, 'STBCAT719680', 'Still Photography', '183854999still-photography.jpg', 'Admin', '2020-06-01 11:46:19', '', '0000-00-00 00:00:00', 1),
(4, 'STBCAT755640', 'Videography', '179093740videography.jpg', 'Admin', '2020-06-01 11:47:35', '', '0000-00-00 00:00:00', 1),
(5, 'STBCAT285618', 'Birthday Photographhy', '720039593birthday-photography.jpg', 'Admin', '2020-06-01 11:50:06', '', '0000-00-00 00:00:00', 1),
(6, 'STBCAT173191', 'Food Photography', '526041829food-photography.jpg', 'Admin', '2020-06-01 11:50:16', '', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `state_id`, `city_name`, `status`) VALUES
(2, 1, 'Rourkela', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `country_name`, `status`) VALUES
(1, 'India', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceid` int(11) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `service_name` varchar(500) NOT NULL,
  `service_desc` varchar(2000) NOT NULL,
  `service_price` bigint(20) NOT NULL,
  `discount_amount` bigint(20) DEFAULT NULL,
  `discount_type` varchar(100) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `approved_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approved_by` varchar(255) NOT NULL,
  `availability` tinyint(4) NOT NULL,
  `reason` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceid`, `service_id`, `category_id`, `vendor_id`, `service_name`, `service_desc`, `service_price`, `discount_amount`, `discount_type`, `created_on`, `created_by`, `updated_on`, `updated_by`, `is_active`, `approved_on`, `approved_by`, `availability`, `reason`) VALUES
(1, 'STBSERDDPXA86V9D', 'STBCAT886154', 'STB367980', 'Wedding Album | Niranjan Photography', '<p>3 years of experience in Wedding Photography.</p>\r\n', 19999, 10, '2', '2020-05-31 08:11:17', 'STB367980', '2020-06-01 03:26:21', 'STB367980', 1, '2020-05-31 11:42:21', 'Admin', 0, ''),
(2, 'STBSERFUT7H9JJN5', 'STBCAT886154', 'STB356753', 'Wedding Dhamaka | Sambhu Photo Studio', '<p>7 Years of Experience in Wedding Photography.</p>\r\n\r\n<p><strong>Low Cost and Efficient.</strong></p>\r\n', 30000, 15, '2', '2020-06-03 04:57:09', 'STB356753', '2020-06-03 04:58:37', 'Admin', 1, '2020-06-03 04:58:37', 'Admin', 0, NULL),
(3, 'STBSERZ4NM643FPM', 'STBCAT173191', 'STB356753', 'Food Dhamaka | Sambhu Photo Studio', '<p>Nice and Efficient Food Photography with Insta Stories!</p>\r\n', 2000, 300, '1', '2020-06-03 04:58:00', 'STB356753', '2020-06-03 04:58:44', 'Admin', 1, '2020-06-03 04:58:44', 'Admin', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active | 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`state_id`, `country_id`, `state_name`, `status`) VALUES
(1, 1, 'Odisha', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `joined_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `user_id`, `name`, `email`, `phone`, `password`, `joined_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(5, 'YRAFG', 'Niranjan Patra', 'niranjanpatra90@gmail.com', 7978019601, '$2y$10$7DDfHLW1lnDBprSth.1FKehg57rNKpfsu5UrIsP7Hj.6CjBqtxAOG', '2020-05-14 04:04:14', NULL, '2020-05-14 11:33:33', 'YRAFG');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `vendorid` int(11) NOT NULL,
  `vendor_id` varchar(255) NOT NULL,
  `bussinessname` varchar(255) NOT NULL,
  `bussinessaddress` varchar(255) NOT NULL,
  `bussinessphone` bigint(20) NOT NULL,
  `mapaddress` varchar(5000) NOT NULL,
  `ownername` varchar(255) NOT NULL,
  `ownernumber` bigint(20) NOT NULL,
  `owneremail` varchar(255) NOT NULL,
  `pancard` varchar(255) NOT NULL,
  `aadhar` varchar(255) NOT NULL,
  `establishmentnumber` varchar(255) DEFAULT NULL,
  `gstinumber` varchar(255) DEFAULT NULL,
  `accountnumber` varchar(255) NOT NULL,
  `bankname` varchar(255) NOT NULL,
  `ifsccode` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `aadharimage` varchar(255) NOT NULL,
  `pancardimage` varchar(255) NOT NULL,
  `bussinesslogo` varchar(255) NOT NULL,
  `country` int(11) NOT NULL,
  `state` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `verified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `password` varchar(255) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`vendorid`, `vendor_id`, `bussinessname`, `bussinessaddress`, `bussinessphone`, `mapaddress`, `ownername`, `ownernumber`, `owneremail`, `pancard`, `aadhar`, `establishmentnumber`, `gstinumber`, `accountnumber`, `bankname`, `ifsccode`, `branch`, `aadharimage`, `pancardimage`, `bussinesslogo`, `country`, `state`, `city`, `created_on`, `created_by`, `updated_on`, `updated_by`, `status`, `verified_on`, `password`, `reason`, `facebook`, `twitter`, `instagram`, `youtube`) VALUES
(2, 'STB367980', 'Niranjan Photography', 'PWD-36, Reserve Police Lane, Rourkela- 769004', 9777788596, 'https://www.niranjanpatra.com', 'Niranjan Patra', 7978019601, 'niranjanpatra90@gmail.com', 'DE10145522', '123456789121', '234R/21/qw', 'GSTI00001111', '36160148302', 'State Bank of India', 'SBIN0007474', 'Uditnagar', '808483234final-day-1.JPG', '160586649final day 2.JPG', '275290502niranjan.jpg', 1, 1, 2, '2020-05-25 02:33:28', 'YRAFG', '2020-05-30 13:51:02', 'STB367980', 1, '2020-05-25 02:35:38', '$2y$10$y6cO8vh6DRXAtYzFsbtrDuZBbta5228.WnZFm3QAvScCTILiqjmJC', '', 'qwefgh', 'wefgh', 'wesfg', 'sfdgh'),
(3, 'STB356753', 'Sambhu Photo Studio', 'Kalinga Housing Colony', 7978607728, 'https://eit-education.com', 'Sambhu Pradhan', 7978607728, 'sambhu_rkl@hotmail.com', 'fg123', '12345678977', 'DE234', 'GST1342', '1234567897', 'State Bank of India', 'SBIN000123', 'Chennd', '949328599final.JPG', '31920741final day 2.JPG', '527292168still-photography.jpg', 1, 1, 2, '2020-06-03 04:52:56', 'YRAFG', '2020-06-03 04:54:42', 'STB356753', 1, '2020-06-03 04:53:39', '$2y$10$zexhvt0C1hIvI6CJ8hyMe.x5.kOq1.9PXo7NIQs8CaBVXnGXZr51a', '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `wishlistid` int(11) NOT NULL,
  `service_id` varchar(255) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(255) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`wishlistid`, `service_id`, `user_id`, `created_on`, `created_by`, `updated_on`, `updated_by`, `is_active`) VALUES
(35, 'STBSERDDPXA86V9D', 'YRAFG', '2020-06-03 06:17:53', 'YRAFG', '0000-00-00 00:00:00', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceid`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`vendorid`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`wishlistid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `serviceid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `vendorid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `wishlistid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
