<?php include 'includes/header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
					
				?>
<body>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Our Studios</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Our Studios</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="shop-area pt-90 pb-90">
            <div class="container">
                <div class="row">
				<?php
				
				$count=1;
										$results_per_page = 12;
										
										if(isset($_GET['country']) && isset($_GET['state']) && isset($_GET['city'])){
											$country = base64_decode(base64_decode(base64_decode($_GET['country'])));
											$state = base64_decode(base64_decode(base64_decode($_GET['state'])));
											$city = base64_decode(base64_decode(base64_decode($_GET['city'])));
											$sqli ="SELECT * FROM vendors WHERE country='$country' AND state = '$state' AND city = '$city' AND status = 1 ORDER BY vendorid DESC";
										}else if(isset($_GET['country']) && isset($_GET['state'])){
											$country = base64_decode(base64_decode(base64_decode($_GET['country'])));
											$state = base64_decode(base64_decode(base64_decode($_GET['state'])));
											$sqli ="SELECT * FROM vendors WHERE country='$country' AND state = '$state' AND status = 1 ORDER BY vendorid DESC";
										}else if(isset($_GET['country'])){
											$country = base64_decode(base64_decode(base64_decode($_GET['country'])));
											$sqli ="SELECT * FROM vendors WHERE country = '$country' AND status = 1 ORDER BY vendorid DESC";
										}else{
											$sqli ="SELECT * FROM vendors WHERE status = 1 ORDER BY vendorid DESC";
										}
										$data = $con->query($sqli);
										$number_of_results = mysqli_num_rows($data);
										$number_of_pages = ceil($number_of_results/$results_per_page);

										
										$this_page_first_result = ($page-1)*$results_per_page;
										if(isset($_GET['country']) && isset($_GET['state']) && isset($_GET['city'])){
											$country = base64_decode(base64_decode(base64_decode($_GET['country'])));
											$state = base64_decode(base64_decode(base64_decode($_GET['state'])));
											$city = base64_decode(base64_decode(base64_decode($_GET['city'])));
											$sql ="SELECT * FROM vendors WHERE country='$country' AND state = '$state' AND city = '$city' AND status = 1 LIMIT " . $this_page_first_result . ',' .  $results_per_page;
										} else if(isset($_GET['country']) && isset($_GET['state'])){
											$country = base64_decode(base64_decode(base64_decode($_GET['country'])));
											$state = base64_decode(base64_decode(base64_decode($_GET['state'])));
											$sql ="SELECT * FROM vendors WHERE country='$country' AND state = '$state' AND status = 1 LIMIT " . $this_page_first_result . ',' .  $results_per_page;
										}else if(isset($_GET['country'])){
											$country = base64_decode(base64_decode(base64_decode($_GET['country'])));
											$sql ="SELECT * FROM vendors WHERE country = '$country' AND status = 1 LIMIT " . $this_page_first_result . ',' .  $results_per_page;
										}else{
											$sql ="SELECT * FROM vendors WHERE status = 1 LIMIT " . $this_page_first_result . ',' .  $results_per_page;
										}
										
										
										
										$result = $con->query($sql);
										if($number_of_results <= $results_per_page){
											$showing = $number_of_results;
										}else{
											$showing = $results_per_page;
										}
				?>
                    <div class="col-lg-12">
                        <div class="shop-topbar-wrapper">
                            <div class="shop-topbar-left">
                                <div class="view-mode nav">
                                    <a class="active" href="#shop-1" data-toggle="tab"><i class="la la-th"></i></a>
                                   
                                </div>
                                <p>Showing <?php if($showing == 0) { echo '0'; } else { echo '1'; } ?> - <?php echo $showing; ?> of <?php echo $number_of_results; ?> results </p>
                            </div>
                            <div class="product-sorting-wrapper">
                              
                                
                            </div>
                        </div>
                        <div class="shop-bottom-area">
                            <div class="tab-content jump">
                                <div id="shop-1" class="tab-pane active">
                                    <div class="row">
									<?php
										
										if($result->num_rows == 0){ ?>
											<center style="font-size: 20px; margin-left: 20px;">No Vendors Found!</center>
											
										<?php }else{ 
										while ($row = $result->fetch_assoc()) { ?>
											
                                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                            <div class="product-wrap mb-35">
                                                <div class="product-img mb-15">
                                                    <a href="vendor-profile.php?vendor_id=<?php echo base64_encode(base64_encode(base64_encode($row['vendor_id']))); ?>"><img src="uploaded_files/bussinesslogo/<?php echo $row['bussinesslogo']; ?>" alt="Vendor"></a>
                                                   
                                                </div>
                                                <div class="product-content">
                                                  
                                                    <h4><a href="vendor-profile.php?vendor_id=<?php echo base64_encode(base64_encode(base64_encode($row['vendor_id']))); ?>"><?php echo $row['bussinessname']; ?></a></h4>
                                                    <a class="btn btn-primary" href="vendor-profile.php?vendor_id=<?php echo base64_encode(base64_encode(base64_encode($row['vendor_id']))); ?>" style="color: white; text-decoration none;">Explore</a>
                                                </div>
                                            </div>
                                        </div>
										
										<?php } } ?>
                                        
                                        
                                    </div>
                                </div>
                                
                                <div class="pagination-style text-center">
								<?php if($showing == 0) { ?>
								
								
								<?php
								} else if(!isset($_GET['country'])) {
								?>
                                    <ul>
									<?php
									   $pageid = $page;
									   if($pageid != 1)
									   {
									   ?>
                                        <li><a class="prev" href="?page=<?php echo $pageid-1; ?>"><i class="la la-angle-left"></i></a></li>
										<?php } else { ?>
   
										   <?php }?>
											<?php
											for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
											?>
                                        <li><a href="?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
                                        <?php } ?>
											<?php
										   $pageid = $page;
										   $pagei = $pagei - 1;
										   if($pageid != $pagei){
										   ?>
                                        <li><a class="next" href="?page=<?php echo $pageid+1; ?>"><i class="la la-angle-right"></i></a></li>
										<?php } else { ?>
   
										<?php } ?>
                                    </ul>
								<?php } else if(isset($_GET['country']) && isset($_GET['state']) && isset($_GET['city'])) { ?>
								<ul>
									<?php
									   $pageid = $page;
									   if($pageid != 1)
									   {
									   ?>
                                        <li><a class="prev" href="?country=<?php echo $_GET['country']; ?>&state=<?php echo $_GET['state']; ?>&city=<?php echo $_GET['city']; ?>&page=<?php echo $pageid-1; ?>"><i class="la la-angle-left"></i></a></li>
										<?php } else { ?>
   
										   <?php }?>
											<?php
											for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
											?>
                                        <li><a href="?country=<?php echo $_GET['country']; ?>&state=<?php echo $_GET['state']; ?>&city=<?php echo $_GET['city']; ?>&page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
                                        <?php } ?>
											<?php
										   $pageid = $page;
										   $pagei = $pagei - 1;
										   if($pageid != $pagei){
										   ?>
                                        <li><a class="next" href="?country=<?php echo $_GET['country']; ?>&state=<?php echo $_GET['state']; ?>&city=<?php echo $_GET['city']; ?>&page=<?php echo $pageid+1; ?>"><i class="la la-angle-right"></i></a></li>
										<?php } else { ?>
   
										<?php } ?>
                                    </ul>
								<?php } else if(isset($_GET['country']) && isset($_GET['state'])) { ?>		
										<ul>
									<?php
									   $pageid = $page;
									   if($pageid != 1)
									   {
									   ?>
                                        <li><a class="prev" href="?country=<?php echo $_GET['country']; ?>&state=<?php echo $_GET['state']; ?>&city=&page=<?php echo $pageid-1; ?>"><i class="la la-angle-left"></i></a></li>
										<?php } else { ?>
   
										   <?php }?>
											<?php
											for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
											?>
                                        <li><a href="?country=<?php echo $_GET['country']; ?>&state=<?php echo $_GET['state']; ?>&city=&page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
                                        <?php } ?>
											<?php
										   $pageid = $page;
										   $pagei = $pagei - 1;
										   if($pageid != $pagei){
										   ?>
                                        <li><a class="next" href="?country=<?php echo $_GET['country']; ?>&state=<?php echo $_GET['state']; ?>&city=&page=<?php echo $pageid+1; ?>"><i class="la la-angle-right"></i></a></li>
										<?php } else { ?>
   
										<?php } ?>
                                    </ul>

								<?php } else if(isset($_GET['country'])){ ?>
										<ul>
									<?php
									   $pageid = $page;
									   if($pageid != 1)
									   {
									   ?>
                                        <li><a class="prev" href="?country=<?php echo $_GET['country']; ?>&state=&city=&page=<?php echo $pageid-1; ?>"><i class="la la-angle-left"></i></a></li>
										<?php } else { ?>
   
										   <?php }?>
											<?php
											for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
											?>
                                        <li><a href="?country=<?php echo $_GET['country']; ?>&state=&city=&page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
                                        <?php } ?>
											<?php
										   $pageid = $page;
										   $pagei = $pagei - 1;
										   if($pageid != $pagei){
										   ?>
                                        <li><a class="next" href="?country=<?php echo $_GET['country']; ?>&state=&city=&page=<?php echo $pageid+1; ?>"><i class="la la-angle-right"></i></a></li>
										<?php } else { ?>
   
										<?php } ?>
                                    </ul>
								<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <?php include 'includes/footer.php'; ?>