     <footer class="footer-area section-padding-2 bg-bluegray pt-80">
            <div class="container-fluid">
                <div class="footer-top pb-40">
                    <div class="row">
                        <div class="col-lg-3 col-md-8 col-12 col-sm-12">
                            <div class="footer-widget mb-30">
                                <a href="index.php"><img src="assets/images/logo/logo.png" width="150px" height="150px" alt="logo"></a>
                                <div class="footer-about">
                                    <p>Studio Bazaar brings you the best and budget friendly Studio at your Fingertips. </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-6 col-sm-6">
                            <div class="footer-widget mb-30 ml-55">
                                <div class="footer-title-3">
                                    <h3>Quick Links</h3>
                                </div>
                                <div class="footer-list-3">
                                    <ul>
                                        <li><a>About US</a></li>
                                        <li><a href="vendors.php">Studios</a></li>
										<?php if(isset($_SESSION['user'])){ ?>
                                        <li><a href="apply_vendor.php">Apply for vendorship</a></li>
										<?php } else { ?>
										<li><a href="auth.php">Apply for vendorship</a></li>
										<?php } ?>
                                        <li><a>Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6 col-sm-6">
                            <div class="footer-widget mb-30 footer-ngtv-mrg1">
                                <div class="footer-title-3">
                                    <h3>Service</h3>
                                </div>
                                <div class="footer-list-3">
                                    <ul>
                                        <li><a href="shop.php">Our Services</a></li>
                                        <li><a>Terms & Conditions</a></li>
                                        <li><a>Privacy Policy</a></li>
                                        <li><a>Disclaimer</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6 col-sm-6">
                            <div class="footer-widget mb-30 ml-35">
                                <div class="footer-title-3">
                                    <h3>Help</h3>
                                </div>
                                <div class="footer-list-3">
                                    <ul>
                                        <li><a>FAQ's</a></li>
                                        <li><a>Refund Policy</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-6 col-sm-6">
                            <div class="footer-widget mb-30 ml-135">
                                <div class="footer-title-3">
                                    <h3>Social Links</h3>
                                </div>
                                <div class="footer-list-3">
                                    <ul>
                                        <li><a>Facebook</a></li>
                                        <li><a>Twitter</a></li>
                                        <li><a>Instagram</a></li>
                                        <li><a>Youtube</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom pt-40 border-top-1">
                    <div class="row">
                        <div class="col-xl-7 col-lg-10 col-md-11 ml-auto mr-auto">
                            <div class="footer-tag-wrap">
                                <div class="footer-tag-title">
                                    <span>Tags :</span>
                                </div>
                                <div class="footer-tag-list">
                                    <ul>
									<?php 
									$ques = "SELECT * FROM categories WHERE is_active = 1";
									$res = $con->query($ques);
									if($res->num_rows == 0) {
										echo "No Categories Found!";
									}else{
										while($reco=$res->fetch_assoc()){
									?>
                                        <li><a href="shop.php?category_id=<?php echo base64_encode(base64_encode(base64_encode($reco['category_id']))); ?>"><?php echo $reco["category_name"]; ?></a></li>
										<?php } } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="copyright-3 text-center pt-20 pb-20 border-top-1">
                        <p>Copyright © <a href="">Studio Bazaar</a>. All Right Reserved</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- JS
============================================ -->

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
    <!-- Modernizer JS -->
    <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
    <!-- Popper JS -->
    <script src="assets/js/vendor/popper.js"></script>
    <!-- Bootstrap JS -->
    <script src="assets/js/vendor/bootstrap.min.js"></script>
	
    <!-- Slick Slider JS -->
    <script src="assets/js/plugins/countdown.js"></script>
    <script src="assets/js/plugins/counterup.js"></script>
    <script src="assets/js/plugins/images-loaded.js"></script>
    <script src="assets/js/plugins/isotope.js"></script>
    <script src="assets/js/plugins/instafeed.js"></script>
    <script src="assets/js/plugins/jquery-ui.js"></script>
    <script src="assets/js/plugins/jquery-ui-touch-punch.js"></script>
    <script src="assets/js/plugins/magnific-popup.js"></script>
    <script src="assets/js/plugins/owl-carousel.js"></script>
    <script src="assets/js/plugins/scrollup.js"></script>
    <script src="assets/js/plugins/waypoints.js"></script>
    <script src="assets/js/plugins/slick.js"></script>
    <script src="assets/js/plugins/wow.js"></script>
    <script src="assets/js/plugins/textillate.js"></script>
    <script src="assets/js/plugins/elevatezoom.js"></script>
    <script src="assets/js/plugins/sticky-sidebar.js"></script>
    <script src="assets/js/plugins/smoothscroll.js"></script>
    <!-- Main JS -->
    <script src="assets/js/main.js"></script>
	<script>
		$(function () {
  $('[data-toggle="popover"]').popover({
	  trigger: 'hover'
  })
})
	</script>
	<script>
$(document).ready(function(){
    $('#country').on('change', function(){
        var countryID = $(this).val();
        if(countryID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'country_id='+countryID,
                success:function(html){
                    $('#state').html(html);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
    $('#state').on('change', function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'ajaxData.php',
                data:'state_id='+stateID,
                success:function(html){
                    $('#city').html(html);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
});
</script>
<script type="text/javascript">
			$(document).ready(function() {
				$('.add-wishlist').on('click', function() {
					var service_id = $(this).data('id');
					if(service_id){
						$.ajax({
							type:'POST',
							url:'addWishlist.php',
							data:'service_id='+service_id,
							success:function(data){
								$("#heart").removeClass('la la-heart-o').addClass('la la-heart');
								new Noty({
									theme: 'sunset',
									type: 'info',
									layout: 'topRight',
									text: data,
									timeout: 3000
								}).show();
							}
						}); 
					}else{
						new Noty({
							theme: 'sunset',
							type: 'error',
							layout: 'topRight',
							text: 'No Service Selected!',
							timeout: 3000
						}).show();
					}
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.delete-wishlist').on('click', function() {
					var wishlistid = $(this).data('id');
					if(wishlistid){
						$.ajax({
							type:'POST',
							url:'deleteWishlist.php',
							data:'wishlistid='+wishlistid,
							success:function(data){
								
								new Noty({
									theme: 'sunset',
									type: 'success',
									layout: 'topRight',
									text: data,
									timeout: 3000
								}).show();
							}
						}); 
					}else{
						new Noty({
							theme: 'sunset',
							type: 'error',
							layout: 'topRight',
							text: 'No Service Selected!',
							timeout: 3000
						}).show();
					}
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.add-cart').on('click', function() {
					var service_id = $(this).data('id');
					var vendor_id = $(this).data('vendor-id');
					
					if(service_id){
						$.ajax({
							type:'POST',
							url:'addCart.php',
							data:'service_id='+service_id+'&vendor_id='+vendor_id,
							success:function(data){
								
								new Noty({
									theme: 'sunset',
									type: 'info',
									layout: 'topRight',
									text: data,
									timeout: 3000
								}).show();
							}
						}); 
					}else{
						new Noty({
							theme: 'sunset',
							type: 'error',
							layout: 'topRight',
							text: 'No Service Selected!',
							timeout: 3000
						}).show();
					}
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('.delete-cart').on('click', function() {
					var cartid = $(this).data('id');
					if(cartid){
						$.ajax({
							type:'POST',
							url:'deleteCart.php',
							data:'cartid='+cartid,
							success:function(data){
								
								new Noty({
									theme: 'sunset',
									type: 'success',
									layout: 'topRight',
									text: data,
									timeout: 3000
								}).show();
							}
						}); 
					}else{
						new Noty({
							theme: 'sunset',
							type: 'error',
							layout: 'topRight',
							text: 'No Service Selected!',
							timeout: 3000
						}).show();
					}
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#clean-cart').on('click', function() {
					var userid = $(this).data('id');
					if(userid){
						$.ajax({
							type:'POST',
							url:'cleanCart.php',
							data:'userid='+userid,
							success:function(data){
								$('#payment').hide();
								new Noty({
									theme: 'sunset',
									type: 'success',
									layout: 'topRight',
									text: data,
									timeout: 3000
								}).show();
							}
						}); 
					}else{
						new Noty({
							theme: 'sunset',
							type: 'error',
							layout: 'topRight',
							text: 'No Service Selected!',
							timeout: 3000
						}).show();
					}
				});
			});
		</script>
		<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
</script>
<script>
	
	$('#npassword, #cpassword').on('keyup', function() {
	  if ($('#npassword').val() == $('#cpassword').val()) {
		$('#error-message').html('Passwords Matching').css('color', 'green');
		$('#changep').prop('disabled', false);
	  } else {
		$('#error-message').html('Password do Not Matching').css('color', 'red');
		$('#changep').prop('disabled', true);
	  }
	});
	</script>
</body>

</html>