<div class="mobile-off-canvas-active">
            <a class="mobile-aside-close"><i class="la la-close"></i></a>
            <div class="header-mobile-aside-wrap">
                
                <div class="mobile-menu-wrap">
                    <!-- mobile menu start -->
                    <div class="mobile-navigation">
                        <!-- mobile menu navigation start -->
                        <nav>
                            <ul class="mobile-menu">
                                <li><a href="index.php">Home</a>
                                   
                                </li>
                                <li><a href="shop.php">Shop</a>
                                   
                                </li>
								 <li><a href="vendors.php">Studios</a>
                                   
                                </li>
                                <li><a href="about.php">About</a>
                                    
                                </li>
                               
                                <li><a href="contact.php">Contact us</a></li>
								<hr>
								<?php if(isset($_SESSION['user'])) { ?>
								 <li><a>Welcome, <?php echo $sesdata['name']; ?></a></li>
								 <li><a href="myaccount.php">My Account</a></li>
								 <li><a href="wishlist.php">Wishlist</a></li>
								 <li><a href="cart.php">My Cart</a></li>
								<?php } else { ?>
									<li><a href="auth.php">Sign In</a></li>
								<?php } ?>
                            </ul>
                        </nav>
                        <!-- mobile menu navigation end -->
                    </div>
                    <!-- mobile menu end -->
                </div>
                
                <div class="mobile-social-wrap">
                    <a class="facebook" href="#"><i class="ti-facebook"></i></a>
                    <a class="twitter" href="#"><i class="ti-twitter-alt"></i></a>
                    <a class="pinterest" href="#"><i class="ti-pinterest"></i></a>
                    <a class="instagram" href="#"><i class="ti-instagram"></i></a>
                    <a class="google" href="#"><i class="ti-google"></i></a>
                </div>
            </div>
        </div>