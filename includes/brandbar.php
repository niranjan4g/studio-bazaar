<div class="header-middle border-top-2 pt-15 pb-15">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-2 col-lg-2">
                                <div class="logo">
                                    <a href="index.php"><img src="assets/images/logo/logo.png" alt="logo" height="70px" width="150px"></a>
                                </div>
                            </div>
                            <div class="col-xl-8 col-lg-7">
                                <div class="header-contact-search-wrap header-contact-search-mrg">
                                    <div class="header-contact-2">
                                        <div class="header-contact-2-icon">
                                            <i class="la la-phone"></i>
                                        </div>
                                        <div class="header-contact-2-text">
                                            <span>Contact</span>
                                            <p>7978019601</p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-xl-2 col-lg-3">
                                <!--div class="cart-wrap cart-wrap-3 cart-wrap-3-white">
                                    <div style="font-size: 35px;">
                                        <i class="la la-facebook"></i>
										<i class="la la-instagram"></i>
										<i class="la la-youtube"></i>
                                      </div>
                                    
                                    
                                </div-->
                            </div>
                        </div>

                    </div>
                </div>