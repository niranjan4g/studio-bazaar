<?php include 'includes/header.php'; ?>
<?php
if(isset($_SESSION['user'])){
	$user_id = $_SESSION['user'];
	$sql = "SELECT * FROM cart WHERE user_id ='$user_id'";
	$result = $con->query($sql);
}else{
	$_SESSION['error'] = 'You are Not Authorized! Please Login';
	header("location: index.php");
	exit();
}
?>
<body>
    <div class="main-wrapper">
         <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Your Cart</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Cart </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="cart-main-area pt-85 pb-90">
            <div class="container">
                <h3 class="cart-page-title">Your cart items</h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        
                            <div class="table-content table-responsive cart-table-content">
                                <table>
                                    <thead>
                                        <tr>
											<th>#</th>
                                            <th>Image</th>
                                            <th>Service Name</th>
											<th>Category Name</th>
											<th>Vendor Name</th>
                                            <th>Service Price</th>
                                            <th>Availabilty</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									$cartstatus = 1;
									$count = 1;
									$result = $con->query("SELECT * FROM cart WHERE user_id = '$user_id'");
									if($result->num_rows == 0) { ?>
										<tr>
										<td colspan="8">No services in Cart!</td>
										</tr>
									<?php }
									while($row = $result->fetch_assoc()){
										$service_id = $row['service_id'];
										$service_array = $con->query("SELECT * FROM services WHERE service_id = '$service_id'")->fetch_assoc();
										$vendor_id = $row['vendor_id'];
										$vendor_array = $con->query("SELECT * FROM vendors WHERE vendor_id = '$vendor_id'")->fetch_assoc();
										$category_id = $service_array['category_id'];
										$category_array = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
										
									?>
                                        <tr>
											<td><?php echo $count++; ?></td>
                                            <td class="product-thumbnail">
                                                <a><img src="management/admin/uploaded_files/category/<?php echo $category_array['category_cover']; ?>" alt="" height="82px" width="82px"></a>
                                            </td>
                                            <td class="product-name"><?php echo $service_array['service_name']; ?></td>
                                            <td class="product-price-cart"><?php echo $category_array['category_name']; ?></td>
                                            <td class="product-quantity">
                                                <?php echo $vendor_array['bussinessname']; ?>
                                            </td>
											 <?php
										$discount = $service_array['discount_amount'];
										$price = $service_array['service_price'];
										$discount_type = $service_array['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
                                            <td class="product-subtotal">₹<?php echo number_format($discount_price, 2); ?></td>
											<?php if($service_array['availability'] == 0) { ?>
											<td>
												Service Available
											</td>
											<?php } else { 
											$cartstatus = 0;
											?>
											<td style="color: red;">
												Service Temporarily Unavailable
											</td>
											<?php } ?>
                                            <td>
                                               
                                                <a href="javascript:void(0)" class="delete-cart" data-id="<?php echo $row['cartid']; ?>"><i class="la la-close"></i></a>
                                            </td>
                                        </tr>
									<?php } ?>
                                    </tbody>
                                </table>
                           
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="cart-shiping-update-wrapper">
                                        <div class="cart-shiping-update">
                                            <a href="shop.php">Continue Shopping</a>
                                        </div>
										<?php if($result->num_rows == 0) {
											
										}else { ?>
                                        <div class="cart-clear">
                                            <a href="javascript:void(0)" id="clean-cart" data-id="<?php echo $user_id; ?>">Clear Shopping Cart</a>
                                        </div>
										<?php } ?>
                                    </div>
                                </div>
                            </div>
                       <?php if($result->num_rows == 0) {
						   
					   } else { ?>
                        <div class="row" id="payment">
                            <div class="col-lg-4 col-md-6">
                               
                            </div>
                            <div class="col-lg-4 col-md-6">
                                
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="grand-totall">
                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gary-cart">Cart Total</h4>
                                    </div>
									<?php
										 $ques = "SELECT * FROM cart WHERE user_id = '$user_id'";
									   $rs = $con->query($ques);
									   $total = $rs->num_rows;
									   $total_sum = 0;
									   while($data = $rs->fetch_assoc()) {
										   $service_id = $data['service_id'];
										   $recc = $con->query("SELECT * FROM services WHERE service_id = '$service_id'")->fetch_assoc();
											   $discount = $recc['discount_amount'];
											$price = $recc['service_price'];
											$discount_type = $recc['discount_type'];
											if($discount_type == 1){
												$discount_price = $price - $discount;
											}else if($discount_type == 2){
												$discount_price = $price - (($discount/100)*$price);
											}else{
												$discount_price = $price;
											}
											$total_sum = $total_sum + $discount_price;
										
									   }
									?>
                                    <h5>Total Services <span><?php echo $total; ?></span></h5>
									<h5>Total Payment <span>₹<?php echo number_format($total_sum, 2); ?></span></h5>
                                    <div class="total-shipping">
                                        <h5>Payment to be Made</h5>
                                        <ul>
                                            <li>Advance Amount<span>10% of ₹<?php echo number_format($total_sum, 2); ?></span></li>
											<?php
											$advance = (0.1)*$total_sum;
											?>
                                            <li>  <span>= ₹<?php echo number_format($advance, 2); ?></span></li>
                                        </ul>
                                    </div>
                                    <h4 class="grand-totall-title">Grand Total <span>₹<?php echo number_format($advance, 2); ?></span></h4>
									<?php if($cartstatus == 1){ ?>
                                    <a href="checkout.php">Proceed to Checkout</a>
									<?php } else { ?>
									<a href="javascript:void(0)" data-toggle="tooltip" title="Some Services are Unavailable. Remove them to Proceed.">Proceed to Checkout</a>
									<?php } ?>
                                </div>
                            </div>
                        </div>
					   <?php } ?>
						
                    </div>
                </div>
            </div>
        </div>
<?php include 'includes/footer.php'; ?>