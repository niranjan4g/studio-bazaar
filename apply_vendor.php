<?php include 'includes/header.php'; ?>
<?Php if(!isset($_SESSION['user'])){
	$_SESSION['error'] = 'You are not Authorized! PLease Login';
	header("location: index.php");
}
?>
<body>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Apply For Vendorship</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Apply for Vendorship </li>
                    </ul>
                </div>
            </div>
        </div>
		
        <div class="login-register-area pt-85 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 ml-auto mr-auto">
                        <div class="login-register-wrapper">
                            <div class="login-register-tab-list nav">
                                <a class="active" data-toggle="tab">
                                    <h4> Apply For Vendorship </h4>
                                </a>
                               
                            </div>
							<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
                           <div class="tab-pane" id="account-info" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Personal Details</h3>
                                                <div class="account-details-form">
                                                    <form action="add_vendor.php" method="POST" enctype="multipart/form-data">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="single-input-item">
                                                                    <label for="first-name" class="required">Name<span style="color: red">*</span></label>
                                                                    <input type="text" id="first-name" name="ownername" required />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="single-input-item">
                                                                    <label for="last-name" class="required">Phone<span style="color: red">*</span></label>
                                                                    <input type="text" id="last-name" name="ownerphone" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="single-input-item">
                                                            <label for="email" class="required">Email Addres<span style="color: red">*</span></label>
                                                            <input type="email" id="email" name="owneremail" required />
                                                        </div>
                                                        <fieldset>
                                                            <legend>Bussiness Details</legend>
                                                            <div class="single-input-item">
                                                                <label for="current-pwd" class="required">Bussiness Name<span style="color: red">*</span></label>
                                                                <input type="text" id="current-pwd" name="bussinessname" required />
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="new-pwd" class="required">Bussiness Contact Number<span style="color: red">*</span></label>
                                                                        <input type="text" id="new-pwd" name="bussinessnumber" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="confirm-pwd" class="required" style="display: inline-block">Address Google Map Link<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Open Google Maps, then click on share button, copy text to clipboard and paste here." style="cursor: pointer;"></i>
                                                                        <input type="text" id="confirm-pwd" name="mapaddress" required />
                                                                    </div>
                                                                </div>
                                                            </div>
															<div class="single-input-item">
                                                                <label for="address" class="required">Bussiness Address<span style="color: red">*</span></label>
                                                                <textarea id="address" name="bussinessaddress" required ></textarea>
                                                            </div>
                                                        </fieldset>
														<fieldset>
                                                            <legend>Address Proofs and Other Details</legend>
                                                            
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="aadhar" class="required" style="display: inline-block">Aadhar Card Number<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Your Aadhar Card Number without spaces." style="cursor: pointer;"></i>
                                                                        <input type="text" id="aadhar" name="aadhar" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="pancard" class="required" style="display: inline-block">Pan Card Number<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Your Pan Card Number without spaces." style="cursor: pointer;"></i>
                                                                        <input type="text" id="pancard" name="pancard" required />
                                                                    </div>
                                                                </div>
                                                            </div>
															<div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="establishment" style="display: inline-block">Bussiness Establishment Number</label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Your Bussiness Establishment Number By Municipality or Any Proof for Bussiness Establishment. You can leave this blank if you dont have one." style="cursor: pointer;"></i>
                                                                        <input type="text" id="establishment" name="establishmentnumber" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="gstin" style="display: inline-block">GSTIN Number</label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter your GST Number here. Leave Black if dont have one." style="cursor: pointer;"></i>
                                                                        <input type="text" id="gstin" name="gstinnumber" required />
                                                                    </div>
                                                                </div>
                                                            </div>
															
                                                        </fieldset>
														<fieldset>
                                                            <legend>Bank Details</legend>
                                                            
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="bankname" class="required" style="display: inline-block">Bank name<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter the Name of the Bank in which you have the account. Prevent Short Forms like SBI, HDFC etc., write full name." style="cursor: pointer;"></i>
                                                                        <input type="text" id="bankname" name="bankname" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="accountnumber" class="required" style="display: inline-block">Account Number<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Your Account Number Correctly." style="cursor: pointer;"></i>
                                                                        <input type="text" id="accountnumber" name="accountnumber" required />
                                                                    </div>
                                                                </div>
                                                            </div>
															<div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="ifsccode" style="display: inline-block">IFSC Code<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter the IFSC Code." style="cursor: pointer;"></i>
                                                                        <input type="text" id="ifsccode" name="ifsccode" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="branch" style="display: inline-block">Branch Name<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter the name of the Place where your Bank Account was created." style="cursor: pointer;"></i>
                                                                        <input type="text" id="branch" name="branch" required />
                                                                    </div>
                                                                </div>
                                                            </div>
															
                                                        </fieldset>
														<fieldset>
                                                            <legend>Documents Upload</legend>
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <div class="single-input-item">
                                                                        <label for="aadharimage" style="display: inline-block">Aadhar Card<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Upload the Front Side of your Aadhar card. The file must be in .jpg/.jpeg/.png format and file should not exceed 5MB. Every Data should be Clearly Visible." style="cursor: pointer;"></i>
                                                                        <input type="file" id="aadharimage" name="aadharimage" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="single-input-item">
                                                                        <label for="pancardimage" style="display: inline-block">Pan Card<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Upload the Front Side of your Pan card. The file must be in .jpg/.jpeg/.png format and file should not exceed 5MB. Every Data should be Clearly Visible." style="cursor: pointer;"></i>
                                                                        <input type="file" id="pancardimage" name="pancardimage" required />
                                                                    </div>
                                                                </div>
																<div class="col-lg-4">
                                                                    <div class="single-input-item">
                                                                        <label for="bussinesslogo" style="display: inline-block">Bussiness Logo<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Upload  your Brand/Bussiness Logo. The file must be in .jpg/.jpeg/.png format and file should not exceed 5MB." style="cursor: pointer;"></i>
                                                                        <input type="file" id="bussinesslogo" name="bussinesslogo" required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
															
															
															
                                                        </fieldset>
														<fieldset>
                                                            <legend>Location Details for Search</legend>
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <div class="single-input-item">
                                                                        <label for="country" style="display: inline-block">Country<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Select your Country" style="cursor: pointer;"></i>
                                                                        <select id="country" class="form-control" name="country">
																			<option value="">Select Country</option>
																			<?php 
																			$query = "SELECT * FROM countries WHERE status = 1 ORDER BY country_name ASC"; 
																			$result = $con->query($query); 
																			if($result->num_rows > 0){ 
																				while($row = $result->fetch_assoc()){  
																					echo '<option value="'.$row['country_id'].'">'.$row['country_name'].'</option>'; 
																				} 
																			}else{ 
																				echo '<option value="">Country not available</option>'; 
																			} 
																			?>
																		</select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div class="single-input-item">
                                                                        <label for="state" style="display: inline-block">State<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Select State" style="cursor: pointer;"></i>
                                                                        <select id="state" class="form-control" name="state">
																			<option value="">Select country first</option>
																		</select>
                                                                    </div>
                                                                </div>
																<div class="col-lg-4">
                                                                    <div class="single-input-item">
                                                                        <label for="city" style="display: inline-block">City<span style="color: red">*</span></label>&nbsp;&nbsp;<i class="la la-question-circle" data-container="body" data-toggle="popover" data-placement="right" data-content="Select City" style="cursor: pointer;"></i>
                                                                        <select id="city" class="form-control" name="city">
																			<option value="">Select state first</option>
																		</select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
															
															
															
                                                        </fieldset>
                                                        <div class="single-input-item">
                                                            <button class="check-btn sqr-btn ">Apply</button>
                                                        </div>
                                                    </form>
                                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>