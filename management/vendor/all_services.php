<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
					$vendor_id = $_SESSION['vendor'];
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Services </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">View Services</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="display: inline;">View Services</h3>
				<a href="add_service.php" style="color: white; text-decoration: none; float: right; font-size: 12px;" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Service</a>
                <div class="card-tools">
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>Service Id</th>
					<th>Category Name</th>
					<th>Service Name</th>
					<th>Service Description</th>
					<th>Service Original Price</th>
					<th>Service Discounted Price</th>
					<th>Created On</th>
                    <th>Status</th>
					<th>Availability</th>
					<th>Reason</th>
					<th>Edit</th>
					
                  </tr>
                  <tr>
                    <?php
                    $count=1;
					$results_per_page = 10;

                    $sqli ="SELECT * FROM services WHERE vendor_id = '$vendor_id' ORDER BY serviceid DESC";
                    $data = $con->query($sqli);
					$number_of_results = mysqli_num_rows($data);
					$number_of_pages = ceil($number_of_results/$results_per_page);

					
                    $this_page_first_result = ($page-1)*$results_per_page;
					$sql="SELECT * FROM services WHERE vendor_id = '$vendor_id' LIMIT " . $this_page_first_result . ',' .  $results_per_page;
					$result = $con->query($sql);
					if($result->num_rows == 0){ ?>
						<center style="font-size: 20px;">No Services Found!</center>
						
					<?php }else{
                      while ($row = $result->fetch_assoc()) {
						  $category_id = $row['category_id'];
						$res = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'");
						$rec = $res->fetch_assoc();
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['service_id'] ?></td>
						<td><?php echo $rec['category_name'] ?></td>
						<td><?php echo $row['service_name']; ?></td>
						<td><?php echo $row['service_desc']; ?></td>
						<td>₹ <?php echo $row['service_price']; ?></td>
						<?php
						$discount = $row['discount_amount'];
						$price = $row['service_price'];
						$discount_type = $row['discount_type'];
						if($discount_type == 1){
							$discount_price = $price - $discount;
						}else if($discount_type == 2){
							$discount_price = $price - (($discount/100)*$price);
						}else{
							$discount_price = $price;
						}
						?>
						<td>₹ <?php echo $discount_price; ?></td>
						<td><?php echo date('d F, Y',strtotime($row['created_on'])); ?></td>
						
						<?php
						if($row['is_active'] == 0) {
						?>
						<td>
						<span class="badge badge-warning">Awaiting Action</span>
						</td>
						<?php } else if($row['is_active'] == 1) { ?>
						<td>
						<span class="badge badge-success">Approved</span>
						</td>
						<?php } else {  ?>
						<td>
						<span class="badge badge-danger">Not Approved</span>
						</td>
						<?php } ?>
						<?php
						if($row['availability'] == 0) { 
						?>
						<td>
						<span class="badge badge-success">Avaialable</span>
						</td>
						<?php } else { ?>
						<td>
						<span class="badge badge-danger">Unavailable</span>
						</td>
						<?php } ?>
						<?php if($row['reason'] == NULL){ ?>
						<td>
							N/A
						</td>
						<?php } else { ?>
						<td>
							<?php echo $row['reason']; ?>
						</td>
						<?Php } ?>
						<td>
						<?php
						if($row['is_active'] == 1) {
						?>
						<a href="edit_service.php?service_id=<?php echo $row['service_id']; ?>" class="nav-link">
                                <i class="nav-icon fa fa-pencil blue"></i>
                              </a>
						<?php } else { ?>
						<?php echo 'N/A'; ?>
						<?php } ?>
						</td>
							 
                        </tr>
                        <?php
                      }
                    }
                  
                 ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
   $pageid = $page;
   if($pageid != 1)
   {
   ?>
    <li class="page-item"><a class="page-link" href="all_services.php?page=<?php echo $pageid-1; ?>">Previous</a></li>
   <?php } else { ?>
   
   <?php }?>
	<?php
	for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
	?>
    <li class="page-item"><a class="page-link" href="all_services.php?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
    <?php } ?>
	<?php
   $pageid = $page;
   $pagei = $pagei - 1;
   if($pageid != $pagei){
   ?>
    <li class="page-item"><a class="page-link" href="all_services.php?page=<?php echo $pageid+1; ?>">Next</a></li>
   <?php } else { ?>
   
   <?php } ?>
  </ul>
</nav>
<br>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
