<?php
include '../db.php'; 
if(isset($_SESSION['vendor'])){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$oldpassword = $_POST["oldpassword"];
		$password = $_POST["password"];
		$cpassword = $_POST["cpassword"];
		
		$vendor_id = $_SESSION['vendor'];
		$sql = "SELECT password FROM vendors WHERE vendor_id = '$vendor_id'";
		//$hashpassword = password_hash($oldpassword, PASSWORD_DEFAULT);
		$result = $con->query($sql);
		$record = $result->fetch_assoc();
		$fetchedpassword = $record["password"];
		
		if(password_verify($oldpassword, $fetchedpassword)){
			if($password == $cpassword){
				$hashedpassword = password_hash($password, PASSWORD_DEFAULT);
				$query = "UPDATE vendors SET password = '$hashedpassword' WHERE vendor_id = '$vendor_id'";
				if($con->query($query) == TRUE){
					$_SESSION['success'] = "Password Updated Successfully!";
					header("location: view_profile.php");
				}else{
					$_SESSION['error'] = "Password wasn't updated! Contact Admin";
					header("location: view_profile.php");
				}
			}else{
				$_SESSION['error'] = "Passwords do not Match!";
				header("location: view_profile.php");
			}
		}else{
			$_SESSION['error'] = "Enter Current Password Correctly!";
			header("location: view_profile.php");
		}
	}
}else{
	$_SESSION['error'] = "You are unauthorized! Please Login";
	header("location: login.php");
}
?>