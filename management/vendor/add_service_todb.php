<?Php
include '../db.php';
if(!isset($_SESSION['vendor'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: login.php");
	exit();
}else{
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$n=10; 
		function getName($n) { 
			$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
			$randomString = 'STBSER'; 
		  
			for ($i = 0; $i < $n; $i++) { 
				$index = rand(0, strlen($characters) - 1); 
				$randomString .= $characters[$index]; 
			} 
		  
			return $randomString; 
		} 
	  
		$service_id = getName($n); 
		$vendor_id = $_SESSION['vendor'];
		$category_id = $_POST["category_id"];
		$service_name = $_POST["service_name"];
		$service_desc = $_POST["service_desc"];
		$service_price = $_POST["service_price"];
		$discount_amount = $_POST["discount_amount"];
		$discount_type = $_POST["discount_type"];
		$availability = $_POST["availability"];
		
		
		$sql = "INSERT INTO services(service_id, category_id, vendor_id, service_name, service_desc, service_price, discount_amount, discount_type, availability, created_on, created_by, is_active) VALUES('$service_id','$category_id', '$vendor_id', '$service_name','$service_desc', '$service_price', '$discount_amount', '$discount_type', '$availability', NOW(), '$vendor_id', 0);";
		if($con->query($sql) == TRUE){
			  $_SESSION['success'] = 'Service added Successfully! You will receive a Confirmation Mail on approval';
			  header("location: all_services.php");
			  exit();
		 } else {
			 $_SESSION['error'] = 'Failed! Contact Developer';
			 header("location: all_services.php");
			 exit();
		 }
	}
	
}
	  
