<?php include 'header.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Service</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Add Service</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Add Service(Services will be published after Admin Approves)</h3>
          </div>
          <!-- /.card-header -->
          <form role="form" action="add_service_todb.php" method="post">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
			   <div class="form-group">
                  <label>Service Category</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control"  name="category_id">
					<option selected disabled>--Select category--</option>
					<?php
					$sqli = "SELECT * FROM categories WHERE is_active = 1";
					$arr = $con->query($sqli);
					while($row = $arr->fetch_assoc()){
					?>
					<option value = "<?php echo $row["category_id"]; ?>"><?php echo $row["category_name"]; ?></option>
					
					
					<?php } ?>
				
					</select>
					</div>
				</div>
			  
			   <div class="form-group">
                  <label>Service Name</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input type="text" class="form-control" name="service_name">
					</div>
				</div>
			  
                <div class="form-group">
                  <label>Service Description</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<textarea class="form-control" name="service_desc" id="content"></textarea>
					</div>
				</div>
				 <div class="form-group">
                  <label>Service Price(in INR)</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input type="number" class="form-control" name="service_price">
					</div>
				</div>
				 <div class="form-group">
                  <label>Discount(in INR or %)</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input type="number" class="form-control" name="discount_amount" id="discount">
					</div>
				</div>
				<div class="form-group" id="unit">
                  <label>Discount Type</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="discount_type">
					<option selected disabled>--Select unit--</option>
					<option value="0">No Discount</option>
					<option value="1">Rupees</option>
					<option value="2">%(Percentage)</option>
					</select>
					</div>
				</div>
				<div class="form-group">
                  <label>Availability</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="availability">
					<option selected disabled>--Select Availability--</option>
					<option value="0">Available</option>
					<option value="1">Unavailable</option>
					
					</select>
					</div>
				</div>
				



                </div>
                <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">

              </div>
              <!-- /.form-group -->

              <!-- /.form-group -->
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-danger">Add Product</button>
            </div>
            </form>

            </div>
            <!-- /.row -->
          </div>
        </div>
       
                <!-- /.col-md-6 -->
              </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->



<?php include 'footer.php'; ?>
