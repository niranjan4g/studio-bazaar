<?Php
include '../db.php';
if(!isset($_SESSION['vendor'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: login.php");
	exit();
}else{
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		
		$service_id = $_POST["service_id"];
		if(empty($service_id)){
			$_SESSION['error'] = 'No Service Selected';
			header("location: all_services.php");
			exit();
		}
		$vendor_id = $_SESSION['vendor'];
		$category_id = $_POST["category_id"];
		$service_name = $_POST["service_name"];
		$service_desc = $_POST["service_desc"];
		$service_price = $_POST["service_price"];
		$discount_amount = $_POST["discount_amount"];
		$discount_type = $_POST["discount_type"];
		$availability = $_POST["availability"];
		
		
		$sql = "UPDATE services SET category_id = '$category_id', service_name = '$service_name', service_desc = '$service_desc', service_price = '$service_price', discount_amount = '$discount_amount', discount_type = '$discount_type', availability = '$availability', updated_on = NOW(), updated_by = '$vendor_id' WHERE service_id = '$service_id'";
		if($con->query($sql) == TRUE){
			  $_SESSION['success'] = 'Service Updated Successfully!';
			  header("location: all_services.php");
			  exit();
		 } else {
			 $_SESSION['error'] = 'Failed! Contact Developer';
			 header("location: all_services.php");
			 exit();
		 }
	}
	
}
	  
