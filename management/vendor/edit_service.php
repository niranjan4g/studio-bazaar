<?php include 'header.php'; ?>
<?php
if(!isset($_GET['service_id'])){
	$_SESSION['error'] = 'Please Select a Service'; ?>
	<script>
	window.open("all_services.php","_self");
	</script>
<?php } else { 
	$service_id = $_GET['service_id'];
	$sql = "SELECT * FROM services WHERE service_id = '$service_id'";
	$result = $con->query($sql);
	$row = $result->fetch_assoc();
	$category_id = $row['category_id'];
	$query = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update Service</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Update Service</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Update Service</h3>
          </div>
          <!-- /.card-header -->
          <form role="form" action="edit_service_todb.php" method="post">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
			  <input type="hidden" name="service_id" value="<?php echo $row["service_id"]; ?>">
			   <div class="form-group">
                  <label>Service Category</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control"  name="category_id">
					<option selected value="<?php echo $row['category_id']; ?>"><?php echo $query['category_name']; ?></option>
					<?php
					$sqli = "SELECT * FROM categories WHERE NOT (category_id = '$category_id') AND is_active = 1";
					$arr = $con->query($sqli);
					while($rec = $arr->fetch_assoc()){
					?>
					<option value = "<?php echo $rec["category_id"]; ?>"><?php echo $rec["category_name"]; ?></option>
					
					
					<?php } ?>
				
					</select>
					</div>
				</div>
			  
			   <div class="form-group">
                  <label>Service Name</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input type="text" class="form-control" name="service_name" value="<?php echo $row['service_name']; ?>">
					</div>
				</div>
			  
                <div class="form-group">
                  <label>Service Description</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<textarea class="form-control" name="service_desc" id="content"><?php echo $row['service_desc']; ?></textarea>
					</div>
				</div>
				 <div class="form-group">
                  <label>Service Price(in INR)</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input type="number" class="form-control" name="service_price" value="<?Php echo $row['service_price']; ?>">
					</div>
				</div>
				 <div class="form-group">
                  <label>Discount(in INR or %)</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input type="number" class="form-control" name="discount_amount" id="discount" value="<?Php echo $row['discount_amount']; ?>">
					</div>
				</div>
				<div class="form-group" id="unit">
                  <label>Discount Type</label>
				  <?php if($row['discount_type'] == 0) { ?>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="discount_type">
					
					<option value="0" selected>No Discount</option>
					<option value="1">Rupees</option>
					<option value="2">%(Percentage)</option>
					</select>
				</div>
				  <?php } else if($row['discount_type'] == 1)  { ?>
				  <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="discount_type">
					
					<option value="0">No Discount</option>
					<option value="1" selected>Rupees</option>
					<option value="2">%(Percentage)</option>
					</select>
				</div>
				  <?php } else { ?>
				  <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="discount_type">
					
					<option value="0">No Discount</option>
					<option value="1">Rupees</option>
					<option value="2" selected>%(Percentage)</option>
					</select>
				</div>
				  <?php } ?>
				  
				</div>
				<div class="form-group">
                  <label>Availability</label>
				  <?php if($row['availability'] == 0) { ?>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="availability">
					
					<option value="0" selected>Available</option>
					<option value="1">Unavailable</option>
					
					</select>
				</div>
				  <?php } else { ?>
				   <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<select class="form-control" name="availability">
					
					<option value="0">Available</option>
					<option value="1" selected>Unavailable</option>
					
					</select>
				</div>
				  <?php } ?>
				</div>
				



                </div>
                <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">

              </div>
              <!-- /.form-group -->

              <!-- /.form-group -->
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-danger">Update Product</button>
            </div>
            </form>

            </div>
            <!-- /.row -->
          </div>
        </div>
       
                <!-- /.col-md-6 -->
              </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->



<?php include 'footer.php'; ?>
