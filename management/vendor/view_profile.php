<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
				?>
  <!-- Content Wrapper. Contains page content -->
  <style>
  body{
    color: #6F8BA4;
}
.section {
    padding: 100px 0;
    position: relative;
}
.gray-bg {
    background-color: #f5f5f5;
}
img {
    max-width: 100%;
}
img {
    vertical-align: middle;
    border-style: none;
}
/* About Me 
---------------------*/
.about-text h3 {
  font-size: 45px;
  font-weight: 700;
  margin: 0 0 6px;
}
@media (max-width: 767px) {
  .about-text h3 {
    font-size: 35px;
  }
}
.about-text h6 {
  font-weight: 600;
  margin-bottom: 15px;
}
@media (max-width: 767px) {
  .about-text h6 {
    font-size: 18px;
  }
}
.about-text p {
  font-size: 18px;
  max-width: 450px;
}
.about-text p mark {
  font-weight: 600;
  color: #20247b;
}

.about-list {
  padding-top: 10px;
}
.about-list .media {
  padding: 5px 0;
}
.about-list label {
  color: #20247b;
  font-weight: 600;
  width: 88px;
  margin: 0;
  position: relative;
}
.about-list label:after {
  content: "";
  position: absolute;
  top: 0;
  bottom: 0;
  right: 11px;
  width: 1px;
  height: 12px;
  background: #20247b;
  -moz-transform: rotate(15deg);
  -o-transform: rotate(15deg);
  -ms-transform: rotate(15deg);
  -webkit-transform: rotate(15deg);
  transform: rotate(15deg);
  margin: auto;
  opacity: 0.5;
}
.about-list p {
  margin: 0;
  font-size: 15px;
}

@media (max-width: 991px) {
  .about-avatar {
    margin-top: 30px;
  }
}

.about-section .counter {
  padding: 22px 20px;
  background: #ffffff;
  border-radius: 10px;
  box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
}
.about-section .counter .count-data {
  margin-top: 10px;
  margin-bottom: 10px;
}
.about-section .counter .count {
  font-weight: 700;
  color: #20247b;
  margin: 0 0 5px;
}
.about-section .counter p {
  font-weight: 600;
  margin: 0;
}
mark {
    background-image: linear-gradient(rgba(252, 83, 86, 0.6), rgba(252, 83, 86, 0.6));
    background-size: 100% 3px;
    background-repeat: no-repeat;
    background-position: 0 bottom;
    background-color: transparent;
    padding: 0;
    color: currentColor;
}
.theme-color {
    color: #fc5356;
}
.dark-color {
    color: #20247b;
}


  </style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Profile </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">View Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        
		
		<section class="section about-section gray-bg" id="about">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-9">
                        <div class="about-text go-to">
                            <h3 class="dark-color">About <?php echo $data['bussinessname']; ?></h3>
                            <h6 class="theme-color lead">VENDOR ID: <?php echo $_SESSION['vendor']; ?></h6>
							 <h6 class="dark-color lead">OWNER NAME: <?php echo $data['ownername']; ?></h6>
                            <p><mark>Address</mark>: <?php echo $data['bussinessaddress']; ?></p>
                            <div class="row about-list">
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>Contact</label>
                                        <p><?php echo $data['bussinessphone']; ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Email</label>
                                        <p><?php echo $data['owneremail']; ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Map Link</label>
                                        <p><?php echo $data['mapaddress']; ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Joined</label>
                                        <p><?php echo date('d F, Y', strtotime($data['verified_on'])); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>Facebook</label>
                                        <p><?php if($data['facebook'] == NULL) { echo 'Not Added'; } else { echo $data['facebook']; } ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Twitter</label>
                                        <p><?php if($data['twitter'] == NULL) { echo 'Not Added'; } else { echo $data['twitter']; } ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Instagram</label>
                                        <p><?php if($data['instagram'] == NULL) { echo 'Not Added'; } else { echo $data['instagram']; } ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Youtube</label>
                                        <p><?php if($data['youtube'] == NULL) { echo 'Not Added'; } else { echo $data['youtube']; } ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="about-avatar">
                            <img src="../../uploaded_files/bussinesslogo/<?php echo $data['bussinesslogo']; ?>" title="" style="border-radius: 50%;" alt="">
                        </div>
                    </div>
                </div>
                <div class="counter">
                    <div class="row">
                        <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h4" data-to="500" data-speed="500">Bussiness Logo</h6>
                                <p class="m-0px font-w-600"><a id="edit_logo" href="#edit_bussiness_logo" data-toggle="modal" data-id="<?php echo $data['vendor_id']; ?>" style="color: white; text-decoration: none;" class="btn btn-success">Update</a></p>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h4" data-to="500" data-speed="500">Bussiness Details</h6>
                                <p class="m-0px font-w-600"><a href="#edit_social_details" data-toggle="modal" data-id="<?php echo $data['vendor_id']; ?>" style="color: white; text-decoration: none;" class="btn btn-success">Update</a></p>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                           <div class="count-data text-center">
                                <h6 class="count h4" data-to="500" data-speed="500">Social Links</h6>
                                <p class="m-0px font-w-600"><a href="#edit_social_links" data-toggle="modal" data-id="<?php echo $data['vendor_id']; ?>" style="color: white; text-decoration: none;" class="btn btn-success">Update</a></p>
                            </div>
                        </div>
                        <div class="col-6 col-lg-3">
                            <div class="count-data text-center">
                                <h6 class="count h4" data-to="500" data-speed="500">Password Settings</h6>
                                <p class="m-0px font-w-600"><a href="#change_password" data-toggle="modal"  style="color: white; text-decoration: none;" class="btn btn-danger">Change Password</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
<script>
$(function(){
  $('#edit_logo').click(function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $('.sliderid').val(id);
  });

});
</script>

<div class="modal fade" id="edit_social_details">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="edit_bussiness_details.php">
                
                <div class="form-group">
                    <div class="col-sm-9">
					<label for="bussinessname">Bussiness Name</label>
                      <input type="text" class="form-control" id="bussinessname" name="bussinessname" value="<?php echo $data['bussinessname']; ?>" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="bussinessphone">Bussiness Contact</label>
                      <input type="text" class="form-control" id="bussinessphone" name="bussinessphone" value="<?php echo $data['bussinessphone']; ?>" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="mapaddress">Google Map Link</label>
                      <input type="text" class="form-control" id="mapaddress" name="mapaddress" value="<?php echo $data['mapaddress']; ?>" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="bussinessaddress">Bussiness Address</label>
                      <textarea class="form-control" id="bussinessaddress" name="bussinessaddress" required><?php echo $data['bussinessaddress']; ?></textarea>
                    </div>
                </div>
				
				 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat">Submit</button>
              </form>
            </div>
        </div>
    </div>
</div>  

<div class="modal fade" id="change_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="change_password.php">
                
                <div class="form-group">
                    <div class="col-sm-9">
					<label for="oldpassword">Current Password</label>
                      <input type="password" class="form-control" id="oldpassword" name="oldpassword" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="password">New Password</label>
                      <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="cpassword">Confirm Password</label>
                      <input type="password" class="form-control" id="cpassword" name="cpassword" required>
                    </div>
                </div>
				
				 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat">Change password</button>
              </form>
            </div>
        </div>
    </div>
</div>  

<div class="modal fade" id="edit_social_links">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="edit_social_links.php">
                
                <div class="form-group">
                    <div class="col-sm-9">
					<label for="facebook">Facebook</label>
                      <input type="text" class="form-control" id="facebook" name="facebook" value="<?php echo $data['facebook']; ?>" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="twitter">Twitter</label>
                      <input type="text" class="form-control" id="twitter" name="twitter" value="<?php echo $data['twitter']; ?>" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="instagram">Instagram</label>
                      <input type="text" class="form-control" id="instagram" name="instagram" value="<?php echo $data['instagram']; ?>" required>
                    </div>
                </div>
				<div class="form-group">
                    <div class="col-sm-9">
					<label for="youtube">Youtube</label>
                      <input type="text" class="form-control" id="youtube" name="youtube" value="<?php echo $data['youtube']; ?>" required>
                    </div>
                </div>
				
				 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat">Submit</button>
              </form>
            </div>
        </div>
    </div>
</div>    

<div class="modal fade" id="edit_bussiness_logo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="edit_bussiness_logo.php" enctype="multipart/form-data">
                <input type="hidden" class="sliderid" name="vendor_id">
                <div class="form-group">
                    <label for="photo" class="col-sm-3 control-label">Bussiness Logo</label>

                    <div class="col-sm-9">
                      <input type="file" id="photo" name="bussinesslogo" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="upload"><i class="fa fa-check-square-o"></i> Update</button>
              </form>
            </div>
        </div>
    </div>
</div>    