<?php
   include('../db.php');
 if(isset($_SESSION['vendor']))
{
	$_SESSION['success'] = 'Logged in Successfully!';
    header('location:index.php');
	exit();
}
	if (isset($_POST['submit'])) {
		
		$username = $con->real_escape_string($_POST['username']);
		$password = $con->real_escape_string($_POST['password']);
		if ($username == "" || $password == "")
		{
			$_SESSION['error'] = 'Please Enter Username/Password';
			header("location: login.php");
			exit();
		}
		else {
			$sql = $con->query("SELECT * FROM vendors WHERE owneremail='$username'");
			if ($sql->num_rows > 0) {
					$data = $sql->fetch_array();
					if (password_verify($password, $data['password'])) {
						$vendorid=$data['vendor_id'];
						$_SESSION['vendor']=$vendorid;
						$_SESSION['success'] = 'Logged In Sucessfully!';
						header('location:index.php');
						exit();
					}
					else {
						$_SESSION['error'] = 'Username/Password does not match';
						header("location: login.php");
						exit();
					}
			}
			$_SESSION['error'] = 'You are not Registered! Please Register';
			header("location: login.php");
			exit();
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Studio Bazar</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../spoc/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../spoc/plugins/iCheck/square/blue.css">
<link rel="shortcut icon" href="logo.jpg">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js"></script>
</head>
<body class="hold-transition login-page">

<div class="login-box">
  <div class="login-logo">
    <a href=""><b>Studio Bazar </b></a>
  </div>
  <?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
	 
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <h4 class="login-box-msg">Vendor Login</h4>

      <form action="login.php" method="post">
        <div class="form-group has-feedback">
          <input type="email" class="form-control" name="username" placeholder="Username">
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" name="password"placeholder="Password">
        </div>
        <div class="row">
        
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
	  <br>
		<span style="float: right;"><a href="forgot-password.php">Forgot Password?</a></span>
     
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="../spoc/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../spoc/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="../spoc/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
