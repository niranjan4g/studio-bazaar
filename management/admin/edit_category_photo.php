<?php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not authorized! Please Login!';
	header("location: login.php");
	exit();
}else{
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		
		$target_dir = "uploaded_files/category/"; //directory to store category images
		$target_file = basename($_FILES["category_file"]["name"]);
		$uploadOk = 1;
		
		if(empty($target_file)){
					$_SESSION['error'] = "Please Upload Category Image";
					header("location: all_categories.php");
					exit();
			}else{
				$random = rand(0,999999999);
				$name_new = ($random.$target_file);
				$imageFileType = strtolower(pathinfo($name_new,PATHINFO_EXTENSION));
					
				if ($_FILES["category_file"]["size"] > 500000) {
					$_SESSION['error'] = "Sorry, your file is too large.";
					$uploadOk = 0;
					header("location: all_categories.php");
					exit();
				}
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
					$_SESSION['error'] = "Sorry, only JPG, JPEG & PNG files are allowed.";
					$uploadOk = 0;
					header("location: all_categories.php");
					exit();
				}
				if ($uploadOk == 0) {
					$_SESSION['error'] = "Sorry, your Document was not uploaded! Contact Admin.";
					header("location: all_categories.php");
					exit();
				} else {
					if (move_uploaded_file($_FILES["category_file"]["tmp_name"], $target_dir.$name_new)) {
						$_SESSION['succcess'] = "Category Image was Successfully Uploaded";
					} else {
						$_SESSION['error'] = "Sorry, there was an error uploading your Category Image.";
						header("location: all_categories.php");
						exit();
					}
				}
				$category_file = $name_new;
			}
		$category_id = $_POST['category_id'];
		
		$sql = "UPDATE categories SET category_cover = '$category_file', updated_by = 'Admin', updated_on = NOW() WHERE category_id = '$category_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Category Photo Updated Successfully!';
			header("location: all_categories.php");
			exit();
		}else{
			$_SESSION['error'] = 'Failed! Contact Developer';
			header("location: all_categories.php");
			exit();
		}
	}else{
		$_SESSION['error'] = 'Not Authorized';
		header("location: all_categories.php");
		exit();
	}
}
?>