<?php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not authorized! Please Login!';
	header("location: login.php");
	exit();
}else{
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$country_id = $_POST['country_id'];
		$state_name = $_POST['state_name'];
		$sql = "INSERT INTO states (country_id, state_name) VALUES ('$country_id','$state_name');";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'State Added Successfully!';
			header("location: all_states.php");
			exit();
		}else{
			$_SESSION['error'] = 'Failed! Contact Developer';
			header("location: all_states.php");
			exit();
		}
	}else{
		$_SESSION['error'] = 'Not Authorized';
		header("location: all_states.php");
		exit();
	}
}
?>