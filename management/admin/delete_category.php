<?Php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: all_categories.php");
	exit();
}else{
	if(isset($_GET['category_id'])){
		$category_id = $_GET['category_id'];
		$sql = "DELETE FROM categories WHERE category_id = '$category_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Category Successfully Deleted';
			header("location: all_categories.php");
			exit();
		}else{
			$_SESSION['error'] = 'Delete Failed! Contact Developer';
			header("location: all_categories.php");
			exit();
		}
	}else{
		$_SESSION['error'] = "Please Select a Category!";
		header("location: all_categories.php");
		exit();		
	}
}
?>