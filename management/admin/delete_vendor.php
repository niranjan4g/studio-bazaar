<?Php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: all_vendors.php");
	exit();
}else{
	if(isset($_GET['vendor_id'])){
		$vendor_id = $_GET['vendor_id'];
		$sql = "DELETE FROM vendors WHERE vendor_id = '$vendor_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Vendor Successfully Deleted';
			header("location: all_vendors.php");
			exit();
		}else{
			$_SESSION['error'] = 'Delete Failed! Contact Developer';
			header("location: all_vendors.php");
			exit();
		}
	}else{
		$_SESSION['error'] = "Please Select a Vendor!";
		header("location: all_vendors.php");
		exit();		
	}
}
?>