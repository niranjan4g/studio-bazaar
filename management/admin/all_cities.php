<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Cities </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">View Cities</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="display: inline;">View Cities</h3>
				<a href="#add" data-toggle="modal" style="color: white; text-decoration: none; float: right; font-size: 12px;" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add City</a>
                <div class="card-tools">
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>Country Name</th>
					<th>State Name</th>
					<th>City Name</th>
                    <th>Status</th>
					<th>Action</th>
                  </tr>
                  <tr>
                    <?php
                    $count=1;
					$results_per_page = 10;

                    $sqli ="SELECT * FROM cities ORDER BY city_id DESC";
                    $data = $con->query($sqli);
					$number_of_results = mysqli_num_rows($data);
					$number_of_pages = ceil($number_of_results/$results_per_page);

					
                    $this_page_first_result = ($page-1)*$results_per_page;
					$sql='SELECT * FROM cities LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					$result = $con->query($sql);
					if($result->num_rows == 0){ ?>
						<center style="font-size: 20px;">No States Found!</center>
						
					<?php }else{
                      while ($row = $result->fetch_assoc()) {
						$state_id = $row['state_id'];
						$query = $con->query("SELECT * FROM states WHERE state_id = '$state_id'");
						$rec = $query->fetch_assoc();
						
						$country_id = $rec['country_id'];
						$query1 = $con->query("SELECT * FROM countries WHERE country_id = '$country_id'");
						$rec1 = $query1->fetch_assoc();
                        ?>
                        <td><?php echo$count++ ?></td>
						<td><?php echo $rec1['country_name']; ?></td>
                        <td><?php echo $rec['state_name'] ?></td>
						<td><?php echo $row['city_name'] ?></td>
						<?php
						if($row['status'] == 0) {
						?>
						<td>
						<span class="badge badge-danger">Inactive</span>
						</td>
						<?php } else { ?>
						<td>
						<span class="badge badge-success">Active</span>
						</td>
						<?php } ?>
						<td>
						<a href="delete_city.php?city_id=<?php echo $row['city_id']; ?>" class="nav-link">
                                <i class="nav-icon fa fa-trash red"></i>
                              </a></td>
                        </tr>
                        <?php
                      }
                    }
                  
                 ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
   $pageid = $page;
   if($pageid != 1)
   {
   ?>
    <li class="page-item"><a class="page-link" href="all_states.php?page=<?php echo $pageid-1; ?>">Previous</a></li>
   <?php } else { ?>
   
   <?php }?>
	<?php
	for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
	?>
    <li class="page-item"><a class="page-link" href="all_states.php?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
    <?php } ?>
	<?php
   $pageid = $page;
   $pagei = $pagei - 1;
   if($pageid != $pagei){
   ?>
    <li class="page-item"><a class="page-link" href="all_states.php?page=<?php echo $pageid+1; ?>">Next</a></li>
   <?php } else { ?>
   
   <?php } ?>
  </ul>
</nav>
<br>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
<div class="modal fade" id="add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="add_city.php">
                <input type="hidden" class="vendorid" name="id">
                <div class="form-group">
                   <div class="col-sm-9">
					<label for="country_name">State Name</label>
					  <select name="state_id" class="form-control">
						<option disabled selected>-Select State Name-</option>
					  <?php
					  $query = $con->query("SELECT * FROM states");
						while($rec = $query->fetch_assoc()) {
					  ?>
						<option value="<?php echo $rec['state_id']; ?>"><?php echo $rec['state_name']; ?></option>
					  <?php } ?>
					  </select>
						
                    </div>
					<br>
                    <div class="col-sm-9">
					<label for="city_name">City Name</label>
                      <input class="form-control" id="city_name" name="city_name" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat">Submit</button>
              </form>
            </div>
        </div>
    </div>
</div>    