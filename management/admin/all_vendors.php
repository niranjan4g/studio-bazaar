<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Vendors </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">View Vendors</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="display: inline;">View Vendors</h3>
				
                <div class="card-tools">
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
					<th>Vendor Id</th>
                    <th>Vendor Name</th>
                    <th>Vendor Email</th>
					<th>Bussiness Name</th>
					<th>Vendor Since</th>
					<th>View</th>
					<th>Delete</th>
                  </tr>
                  <tr>
                    <?php
                    $count=1;
					$results_per_page = 10;

                    $sqli ="SELECT * FROM vendors WHERE status = 1 ORDER BY vendorid DESC";
                    $data = $con->query($sqli);
					$number_of_results = mysqli_num_rows($data);
					$number_of_pages = ceil($number_of_results/$results_per_page);

					
                    $this_page_first_result = ($page-1)*$results_per_page;
					$sql='SELECT * FROM vendors WHERE status = 1 LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					$result = $con->query($sql);
					if($result->num_rows == 0){ ?>
						<center style="font-size: 20px;">No Vendors Found!</center>
						
					<?php }else{
                      while ($row = $result->fetch_assoc()) {
						
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['vendor_id'] ?></td>
						<td><?php echo $row['ownername'] ?></td>
						<td><?php echo $row['owneremail'] ?></td>
						<td><?php echo $row['bussinessname'] ?></td>
						<td><?php echo date('d F, Y', strtotime($row['verified_on'])); ?></td>
						<td><a href="view_vendor.php?vendor_id=<?Php echo $row['vendor_id']; ?>" class="nav-link"><i class="nav-icon fa fa-eye blue"></i></a></td>
						<td>
						<a href="delete_vendor.php?vendor_id=<?php echo $row['vendor_id']; ?>" class="nav-link">
                                <i class="nav-icon fa fa-trash red"></i>
                              </a></td>
                        </tr>
                        <?php
                      }
                    }
                  
                 ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
   $pageid = $page;
   if($pageid != 1)
   {
   ?>
    <li class="page-item"><a class="page-link" href="all_vendors.php?page=<?php echo $pageid-1; ?>">Previous</a></li>
   <?php } else { ?>
   
   <?php }?>
	<?php
	for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
	?>
    <li class="page-item"><a class="page-link" href="all_vendors.php?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
    <?php } ?>
	<?php
   $pageid = $page;
   $pagei = $pagei - 1;
   if($pageid != $pagei){
   ?>
    <li class="page-item"><a class="page-link" href="all_vendors.php?page=<?php echo $pageid+1; ?>">Next</a></li>
   <?php } else { ?>
   
   <?php } ?>
  </ul>
</nav>
<br>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
