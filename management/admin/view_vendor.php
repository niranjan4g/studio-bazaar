<?php include 'header.php'; ?>
<?php
if (!isset($_GET['vendor_id'])) {
	$_SESSION['error'] = 'Please Select a vendor Application to view!';
  header('Location: all_vendors.php');
  exit();
}
 else{

 $vendor_id = $_GET['vendor_id'];
 $sql = $con->query("SELECT * FROM vendors  WHERE vendor_id='$vendor_id' ");
 if($sql->num_rows == 0){
	 $_SESSION['error'] = 'No Vendor Applications found!';
	 header("location: all_vendors.php");
	 exit();
 }else{
 $data = $sql->fetch_array();
 }
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Vendor Details </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Vendor Details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                

                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">
                  </h3>
                
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
				<li class="nav-item">
                    <a href="#" class="nav-link">
                       Vendor Id<span class="float-right badge bg-success"><?php echo $data['vendor_id']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                       Joined On<span class="float-right badge bg-danger"><?php echo date('d F, Y', strtotime($data['verified_on'])); ?></span>
                    </a>
                  </li>
				<li class="nav-item">
                    <a href="#" class="nav-link">
                       Bussiness Name<span class="float-right badge bg-warning"><?php echo $data['bussinessname']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Bussiness Address <span class="float-right badge bg-success"><?php echo $data['bussinessaddress']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Bussiness Contact Number <span class="float-right badge bg-primary"><?php echo $data['bussinessphone']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo $data['mapaddress']; ?>" target="_blank" class="nav-link">
                      Bussiness Address Google Map Link <span class="float-right badge bg-info"><?php echo $data['mapaddress']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Owner Name <span class="float-right badge bg-success"><?php echo $data['ownername']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Owner Email ID <span class="float-right badge bg-success"><?php echo $data['owneremail']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Owner's Phone Number <span class="float-right badge bg-info"><?php echo $data['ownernumber']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Pan Card Number <span class="float-right badge bg-primary"><?php echo $data['pancard']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Aadhar Card Number <span class="float-right badge bg-danger"><?php echo $data['aadhar']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Company Establishment Number <span class="float-right badge bg-primary"><?php echo $data['establishmentnumber']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      GSTIN Number <span class="float-right badge bg-success"><?php echo $data['gstinumber']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Bank Name <span class="float-right badge bg-warning"><?php echo $data['bankname']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Account Number <span class="float-right badge bg-info"><?php echo $data['accountnumber']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      IFSC Code <span class="float-right badge bg-primary"><?php echo $data['ifsccode']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Branch Name <span class="float-right badge bg-danger"><?php echo $data['branch']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Aadhar Card <span class="float-right"><img data-toggle="modal" data-target="#aadharimage" src="../../uploaded_files/aadharcard/<?php echo $data['aadharimage']; ?>" width="200px" height="200px" /></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Pancard <span class="float-right"><img data-toggle="modal" data-target="#pancardimage" src="../../uploaded_files/pancard/<?php echo $data['pancardimage']; ?>" width="200px" height="200px"/></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Passbook <span class="float-right"><img data-toggle="modal" data-target="#bussinesslogo" src="../../uploaded_files/bussinesslogo/<?php echo $data['bussinesslogo']; ?>" width="200px" height="200px"/></span>
                    </a>
                  </li>
				    
				
                 
                </ul>
			
              </div>
			  
            </div>
            <!-- /.widget-user -->
          </div>
</div>

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <div id="aadharimage" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <img src="../../uploaded_files/aadharcard/<?php echo $data['aadharimage']; ?>" width="100%" class="img-responsive">
        </div>
    </div>
  </div>
</div>
<div id="pancardimage" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <img src="../../uploaded_files/pancard/<?php echo $data['pancardimage']; ?>"  width="100%" class="img-responsive">
        </div>
    </div>
  </div>
</div>
<div id="bussinesslogo" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-body">
            <img src="../../uploaded_files/bussinesslogo/<?php echo $data['bussinesslogo']; ?>" width="100%" class="img-responsive">
        </div>
    </div>
  </div>
</div>
  

  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>

