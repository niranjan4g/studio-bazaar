<?Php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: all_countries.php");
	exit();
}else{
	if(isset($_GET['country_id'])){
		$country_id = $_GET['country_id'];
		$sql = "DELETE FROM countries WHERE country_id = '$country_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Country Successfully Deleted';
			header("location: all_countries.php");
			exit();
		}else{
			$_SESSION['error'] = 'Delete Failed! Contact Developer';
			header("location: all_countries.php");
			exit();
		}
	}else{
		$_SESSION['error'] = "Please Select a Country!";
		header("location: all_countries.php");
		exit();		
	}
}
?>