<?php include 'header.php'; ?>
<?php
if (!isset($_GET['applicationid'])) {
  header('Location: index.php');
}
 else{
   include('../db.php');
 $applicationid = $_GET['applicationid'];
 $sql = $con->query("SELECT * FROM form  WHERE applicationid='$applicationid' ");
 $data = $sql->fetch_array();
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">USER DETAILS </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">User details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-warning">
                <div class="widget-user-image">
                  <img class="img-square elevation-3"> <?php if($data['photo'] != ""): ?>
									<img src="../../photo/<?php echo $data['photo'];?>" width="100px" height="100px" style="border:1px solid #333333;float:left; display: inline;">
									<?php else: ?>
									<img src="../img/default.png" width="100px" height="100px" style="border:1px solid #333333;">
									<?php endif; ?>
                </div>

                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">
                  <?php
                  if ($data['email']=="") {
                  }
                  else {
                     echo strtoupper($data['firstname']) ;
					 echo strtoupper($data['lastname']) ;
                  }
                   ?> </h3>
                
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
				<li class="nav-item">
                    <a href="#" class="nav-link">
                      Application ID <span class="float-right badge bg-warning"><?php echo $data['applicationid']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      First Name <span class="float-right badge bg-success"><?php echo $data['firstname']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Last Name <span class="float-right badge bg-primary"><?php echo $data['lastname']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Mobile <span class="float-right badge bg-info"><?php echo $data['phone']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Email ID <span class="float-right badge bg-success"><?php echo $data['email']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Father's Name <span class="float-right badge bg-success"><?php echo $data['fathername']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Date of Birth <span class="float-right badge bg-info"><?php echo $data['dob']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Annual Income <span class="float-right badge bg-primary"><?php echo $data['annualincome']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      State <span class="float-right badge bg-danger"><?php echo $data['state']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      District <span class="float-right badge bg-primary"><?php echo $data['district']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      City <span class="float-right badge bg-success"><?php echo $data['city']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Pincode <span class="float-right badge bg-warning"><?php echo $data['pincode']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Address <span class="float-right badge bg-info"><?php echo $data['address']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Aadhar Card <span class="float-right"><img src="../../aadhar/<?php echo $data['aadhar']; ?>" width="100px" height="100px" /></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Pancard <span class="float-right"><img src="../../pancard/<?php echo $data['pancard']; ?>" width="100px" height="100px"/></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Passbook <span class="float-right"><img src="../../passbook/<?php echo $data['passbook']; ?>" width="100ps" height="100px"/></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      Country Access <span class="float-right badge bg-primary"><?php echo $data['country']; ?></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      Region Access <span class="float-right badge bg-info"><?php echo $data['region']; ?></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      Place Access <span class="float-right badge bg-warning"><?php echo $data['place']; ?></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      Ip Address <span class="float-right badge bg-danger"><?php echo $data['ipaddress']; ?></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      Latitude Access <span class="float-right badge bg-success"><?php echo $data['lat']; ?></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      Longitude Access <span class="float-right badge bg-primary"><?php echo $data['lng']; ?></span>
                    </a>
                  </li>
				    <li class="nav-item">
                    <a href="#" class="nav-link">
                      User Location <div class="float-right" id="map" style="height: 400px; width: 100%;"></div>
                    </a>
                  </li>
				
                 
                </ul>
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
</div>

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->
<script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: <?php echo $data["lat"]; ?>, lng: <?php echo $data["lng"]; ?>};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 14, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
     <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtUmkoqsmGSa-k15hhbr75lEj1r-ItKzE&v=3&callback=initMap"></script>    
<?php include 'footer.php'; ?>
