<?php
include '../db.php'; 
if(isset($_SESSION['admin'])){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$oldpassword = $_POST["oldpassword"];
		$password = $_POST["password"];
		$cpassword = $_POST["cpassword"];
		
		$id = $_SESSION['admin'];
		$sql = "SELECT password FROM admin WHERE id = '$id'";
		//$hashpassword = password_hash($oldpassword, PASSWORD_DEFAULT);
		$result = $con->query($sql);
		$record = $result->fetch_assoc();
		$fetchedpassword = $record["password"];
		
		if(password_verify($oldpassword, $fetchedpassword)){
			if($password == $cpassword){
				$hashedpassword = password_hash($password, PASSWORD_DEFAULT);
				$query = "UPDATE admin SET password = '$hashedpassword' WHERE id = '$id'";
				if($con->query($query) == TRUE){
					$_SESSION['success'] = "Password Updated Successfully!";
					header("location: c_pass.php");
				}else{
					$_SESSION['error'] = "Password wasn't updated! Contact Admin";
					header("location: c_pass.php");
				}
			}else{
				$_SESSION['error'] = "Passwords do not Match!";
				header("location: c_pass.php");
			}
		}else{
			$_SESSION['error'] = "Enter Current Password Correctly!";
			header("location: c_pass.php");
		}
	}
}else{
	$_SESSION['error'] = "You are unauthorized! Please Login";
	header("location: login.php");
}
?>