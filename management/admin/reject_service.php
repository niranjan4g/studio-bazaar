<?php
include '../db.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

if(isset($_SESSION['admin'])){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$service_id = $_POST['id'];
		$reason = $_POST['reason'];
		$query = $con->query("SELECT * FROM services WHERE service_id = '$service_id' AND is_active = 0");
		if($query->num_rows == 0){
			$_SESSION['error'] = 'No Service Found!';
			header("location: service_requests.php");
			exit();
		}else{
			$array = $query->fetch_assoc();
			$vendor_id = $array['vendor_id'];
			$record = $con->query("SELECT * FROM vendors WHERE vendor_id = '$vendor_id'")->fetch_assoc();
			$owneremail = $record['owneremail'];
			
			$sql = "UPDATE services SET is_active = 2, updated_by = 'Admin', updated_on = NOW(), reason = '$reason' WHERE service_id = '$service_id'";
			//code to send mail and execute query
			$mail = new PHPMailer(true);
			
			//Server settings
			$mail->SMTPDebug = 2;                                       // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  						// Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'tech.ecommerceeit@gmail.com';               // SMTP username
			$mail->Password   = 'z0Th@n345';                            // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('tech.ecommerceeit@gmail.com');
			$mail->addAddress($owneremail);     						// Add a recipient
			

			// Content
			$mail->isHTML(true);                                  		// Set email format to HTML
			$mail->Subject = 'Unpublish Notification';
			$mail->Body    = '<p>Dear '.$record['ownername'].'!</p><p>Sorry, your Service Named <b>'.$array['service_name'].'</b> was not Published due to the following reason.</p><p><b>Reason:</b><br><br>'.$reason.'</p><p>Please Reply to this mail for queries or re-apply.</p>';
			

			if($mail->send()){
				if($con->query($sql) == TRUE){
					$_SESSION['success'] = "The Service is Rejected! Check Applications";
					header("location: service_requests.php");
					exit();
				}else{
					$_SESSION['error'] = "You Service did not get Rejected! Contact Developers";
					header("location: index.php");
					exit();
				}
			}else{
				$_SESSION['error'] = 'Mail Could not be Sent! The email was incorrect';
				header("location: index.php");
				exit();
			}
		}
	}
}else{
	$_SESSION['error'] = 'You are not authorized! Please Login';
	header("location: login.php");
	exit();
}
?>