<?php include 'header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
				?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">View Categories </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">View Categories</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
	<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="display: inline;">View Categories</h3>
				<a href="#add" data-toggle="modal" style="color: white; text-decoration: none; float: right; font-size: 12px;" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Category</a>
                <div class="card-tools">
                 
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                  <tr>
                    <th>#</th>
                    <th>Category Id</th>
					<th>Category Name</th>
					<th>Category Image</th>
					<th>Created On</th>
                    <th>Status</th>
					<th>Edit</th>
					<th>Delete</th>
                  </tr>
                  <tr>
                    <?php
                    $count=1;
					$results_per_page = 10;

                    $sqli ="SELECT * FROM categories ORDER BY catid DESC";
                    $data = $con->query($sqli);
					$number_of_results = mysqli_num_rows($data);
					$number_of_pages = ceil($number_of_results/$results_per_page);

					
                    $this_page_first_result = ($page-1)*$results_per_page;
					$sql='SELECT * FROM categories LIMIT ' . $this_page_first_result . ',' .  $results_per_page;
					$result = $con->query($sql);
					if($result->num_rows == 0){ ?>
						<center style="font-size: 20px;">No Categories Found!</center>
						
					<?php }else{
                      while ($row = $result->fetch_assoc()) {
						
                        ?>
                        <td><?php echo$count++ ?></td>
                        <td><?php echo $row['category_id'] ?></td>
						<td><?php echo $row['category_name'] ?></td>
						<td><img src="uploaded_files/category/<?php echo $row['category_cover'] ?>" width="100px" height="100px" />&nbsp;&nbsp;<a href="#edit_photo" data-toggle="modal" class="photo" data-id="<?php echo $row['category_id']; ?>"><span class="fa fa-edit"></span></a></i></td>
						<td><?php echo date('d F, Y', strtotime($row['category_id'])); ?></td>
						<?php
						if($row['is_active'] == 0) {
						?>
						<td>
						<span class="badge badge-danger">Inactive</span>
						</td>
						<?php } else { ?>
						<td>
						<span class="badge badge-success">Active</span>
						</td>
						<?php } ?>
						<td>
						<a href="edit_category.php?category_id=<?php echo $row['category_id']; ?>" class="nav-link">
                                <i class="nav-icon fa fa-pencil blue"></i>
                              </a>
						</td>
							  <td>
						<a href="delete_category.php?category_id=<?php echo $row['category_id']; ?>" class="nav-link">
                                <i class="nav-icon fa fa-trash red"></i>
                              </a></td>
                        </tr>
                        <?php
                      }
                    }
                  
                 ?>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div><!-- /.row -->
		<nav aria-label="Page navigation example">
  <ul class="pagination">
    <?php
   $pageid = $page;
   if($pageid != 1)
   {
   ?>
    <li class="page-item"><a class="page-link" href="all_categories.php?page=<?php echo $pageid-1; ?>">Previous</a></li>
   <?php } else { ?>
   
   <?php }?>
	<?php
	for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
	?>
    <li class="page-item"><a class="page-link" href="all_categories.php?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
    <?php } ?>
	<?php
   $pageid = $page;
   $pagei = $pagei - 1;
   if($pageid != $pagei){
   ?>
    <li class="page-item"><a class="page-link" href="all_categories.php?page=<?php echo $pageid+1; ?>">Next</a></li>
   <?php } else { ?>
   
   <?php } ?>
  </ul>
</nav>
<br>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
<script>
$(function(){
  $('.photo').click(function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $('.sliderid').val(id);
  });

});
</script>

<div class="modal fade" id="add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="add_category.php" enctype="multipart/form-data">
                
                <div class="form-group">
                    <div class="col-sm-9">
					<label for="category_name">Category Name</label>
                      <input type="text" class="form-control" id="category_name" name="category_name" required>
                    </div>
                </div>
				
				 <div class="form-group">
                    <div class="col-sm-9">
					<label for="category_file">Category Image</label>
                      <input type="file" class="form-control" id="category_file" name="category_file" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat">Submit</button>
              </form>
            </div>
        </div>
    </div>
</div>    

<div class="modal fade" id="edit_photo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="edit_category_photo.php" enctype="multipart/form-data">
                <input type="hidden" class="sliderid" name="category_id">
                <div class="form-group">
                    <label for="photo" class="col-sm-3 control-label">Photo</label>

                    <div class="col-sm-9">
                      <input type="file" id="photo" name="category_file" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="upload"><i class="fa fa-check-square-o"></i> Update</button>
              </form>
            </div>
        </div>
    </div>
</div>    