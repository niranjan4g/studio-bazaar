<?php
include '../db.php';

if(isset($_SESSION['admin'])){
	if(isset($_GET['service_id'])){
		$service_id =$_GET['service_id'];
		$query = $con->query("SELECT * FROM services WHERE service_id = '$service_id' AND is_active = 0");
		if($query->num_rows == 0){
			$_SESSION['error'] = 'No Service Found';
			header('location: view_service.php');
			exit();
		}else{
			
			$sql = "UPDATE services SET is_active = 1, updated_by = 'Admin', updated_on = NOW(), approved_on = NOW(), approved_by = 'Admin' WHERE service_id = '$service_id'";
			
			
			
				if($con->query($sql) == TRUE){
					$_SESSION['success'] = "The Service is Published! Check Published Services";
					header("location: all_services.php");
					exit();
				}else{
					$_SESSION['error'] = "You Servcie did not get Published! Contact Developers";
					header("location: index.php");
					exit();
				}
			
		}	
	}else{
		$_SESSION['error'] = 'Please Select an Service';
		header("location: view_service.php");
		exit();
	}
}else{
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: login.php");
	exit();
}
?>