<?php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not authorized! Please Login!';
	header("location: login.php");
	exit();
}else{
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$state_id = $_POST['state_id'];
		$city_name = $_POST['city_name'];
		$sql = "INSERT INTO cities (state_id, city_name) VALUES ('$state_id','$city_name');";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'State Added Successfully!';
			header("location: all_cities.php");
			exit();
		}else{
			$_SESSION['error'] = 'Failed! Contact Developer';
			header("location: all_cities.php");
			exit();
		}
	}else{
		$_SESSION['error'] = 'Not Authorized';
		header("location: all_cities.php");
		exit();
	}
}
?>