<?php include 'header.php'; ?>
<?php
if (!isset($_GET['service_id'])) {
	$_SESSION['error'] = 'Please Select a Service to view!';
  header('Location: service_requests.php');
  exit();
}
 else{

 $service_id = $_GET['service_id'];
 $sql = $con->query("SELECT * FROM services  WHERE service_id='$service_id' ");
 if($sql->num_rows == 0){
	 $_SESSION['error'] = 'No Service Requests found!';
	 header("location: service_requests.php");
	 exit();
 }else{
 $data = $sql->fetch_array();
 }
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Service Request Details </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Service Request Details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <!-- Widget: user widget style 2 -->
            <div class="card card-widget widget-user-2">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-info">
                

                <!-- /.widget-user-image -->
                <h3 class="widget-user-username">
                  </h3>
                
              </div>
              <div class="card-footer p-0">
                <ul class="nav flex-column">
				<li class="nav-item">
                    <a href="#" class="nav-link">
                       Service ID<span class="float-right badge bg-warning"><?php echo $data['service_id']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Category ID <span class="float-right badge bg-success"><?php echo $data['category_id']; ?></span>
                    </a>
                  </li>
				  <?php
				  $category_id = $data['category_id'];
				  $rec = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
				  ?>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Category Name <span class="float-right badge bg-primary"><?php echo $rec['category_name']; ?></span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Vendor ID<span class="float-right badge bg-info"><?php echo $data['vendor_id']; ?></span>
                    </a>
                  </li>
				  <?php
				  $vendor_id = $data['vendor_id'];
				  $recc = $con->query("SELECT * FROM vendors WHERE vendor_id = '$vendor_id'")->fetch_assoc();
				  ?>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Vendor Name <span class="float-right badge bg-success"><?php echo $recc['ownername']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Service Name <span class="float-right badge bg-primary"><?php echo $data['service_name']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Service Description <span class="float-right badge bg-info"><?php echo $data['service_desc']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Service Price <span class="float-right badge bg-primary">₹<?php echo $data['service_price']; ?></span>
                    </a>
                  </li>
				  
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Discount <span class="float-right badge bg-danger">
					  <?php if($data['discount_type'] == 1) { ?>
						  ₹<?php echo $data['discount_amount']; ?>
						<?php  } else if($data['discount_type'] == 2) { ?>
							 <?php echo $data['discount_amount']; ?>%
							<?php  } else { ?>
							  ₹0
							 <?php } ?></span>
                    </a>
                  </li>
				  <?php
						$discount = $data['discount_amount'];
						$price = $data['service_price'];
						$discount_type = $data['discount_type'];
						if($discount_type == 1){
							$discount_price = $price - $discount;
						}else if($discount_type == 2){
							$discount_price = $price - (($discount/100)*$price);
						}else{
							$discount_price = $price;
						}
						?>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Discounted Price <span class="float-right badge bg-primary">₹<?php echo $discount_price; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Admin Share <span class="float-right badge bg-success">₹<?php 
						$admin_share = 0.1*$discount_price;
						echo $admin_share;
					  ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Availability <span class="float-right badge bg-warning"><?php  
							if($data['availability'] == 0) { ?>
							 Available
							<?php } else { ?>
							Not Available
							<?php } ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                    <a href="#" class="nav-link">
                      Created On <span class="float-right badge bg-info"><?php echo date('d F, Y', strtotime($data['created_on'])); ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Created By<span class="float-right badge bg-primary"><?php echo $data['created_by']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Updated On <span class="float-right badge bg-danger"><?php echo date('d F, Y', strtotime($data['updated_on'])); ?></span>
                    </a>
                  </li>
				   
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Updated By <span class="float-right badge bg-info"><?php echo $data['updated_by']; ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Approved On <span class="float-right badge bg-primary"><?php echo date('d F, Y', strtotime($data['approved_on'])); ?></span>
                    </a>
                  </li>
				  <li class="nav-item">
                    <a href="#" class="nav-link">
                      Approved By<span class="float-right badge bg-danger"><?php echo $data['approved_by']; ?></span>
                    </a>
                  </li>
				   <li class="nav-item">
                   <a href="#" class="nav-link">
                     
                    </a>
                  </li>
				    
				
                 
                </ul>
				<?php if($data['is_active'] == 0) { ?>
				<div>
				<center><a class="btn btn-success" href="publish_service.php?service_id=<?php echo $data['service_id']; ?>" style="color: white; text-decoration: none; margin-bottom: 10px;">Accept</a>
				
				<a href="#reason" data-toggle="modal" style="color: white; text-decoration: none; margin-bottom: 10px;" class="btn btn-danger reject" data-id="<?php echo $data['service_id']; ?>">Reject</a></center>
				</div>
				<?php } ?>
              </div>
			  
            </div>
            <!-- /.widget-user -->
          </div>
</div>

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  
<div class="modal fade" id="reason">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><b><span class="del_employee_name"></span></b></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" method="POST" action="reject_service.php" enctype="multipart/form-data">
                <input type="hidden" class="vendorid" name="id">
                <div class="form-group">
                    <label for="photo" class="col-sm-3 control-label">Reason</label>

                    <div class="col-sm-9">
                      <textarea id="photo" class="form-control" name="reason" required></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
              <button type="submit" class="btn btn-success btn-flat" name="upload"><i class="fa fa-check-square-o"></i> Submit</button>
              </form>
            </div>
        </div>
    </div>
</div>    

  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

<?php include 'footer.php'; ?>
<script>
$(function(){
  

  
  $('.reject').click(function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $('.vendorid').val(id);
  });

});
</script>
