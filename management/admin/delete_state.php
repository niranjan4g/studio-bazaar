<?Php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: all_states.php");
	exit();
}else{
	if(isset($_GET['state_id'])){
		$state_id = $_GET['state_id'];
		$sql = "DELETE FROM states WHERE state_id = '$state_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'State Successfully Deleted';
			header("location: all_states.php");
			exit();
		}else{
			$_SESSION['error'] = 'Delete Failed! Contact Developer';
			header("location: all_states.php");
			exit();
		}
	}else{
		$_SESSION['error'] = "Please Select a State!";
		header("location: all_states.php");
		exit();		
	}
}
?>