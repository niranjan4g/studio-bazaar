<?php
include '../db.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

if(isset($_SESSION['admin'])){
	if(isset($_GET['vendorid'])){
		$vendorid = base64_decode(base64_decode(base64_decode($_GET['vendorid'])));
		$query = $con->query("SELECT * FROM vendors WHERE vendorid = '$vendorid' and status = 0");
		if($query->num_rows == 0){
			$_SESSION['error'] = 'No Application Found';
			header('location: view_application.php');
			exit();
		}else{
			$array = $query->fetch_assoc();
			$owneremail = $array['owneremail'];
			
			$n=6; 
			function getName($n) { 
				$characters = '0123456789'; 
				$randomString = 'STB'; 
		  
				for ($i = 0; $i < $n; $i++) { 
					$index = rand(0, strlen($characters) - 1); 
					$randomString .= $characters[$index]; 
				} 
				return $randomString; 
			}
			$vendor_id = getName($n); 
			$password = password_hash($vendor_id, PASSWORD_DEFAULT);
			$sql = "UPDATE vendors SET vendor_id = '$vendor_id', status = 1, password = '$password', updated_by = 'Admin', updated_on = NOW(), verified_on = NOW() WHERE vendorid = '$vendorid'";
			
			//code to send mail and execute query
			$mail = new PHPMailer(true);
			
			//Server settings
			$mail->SMTPDebug = 2;                                       // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  						// Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'tech.ecommerceeit@gmail.com';               // SMTP username
			$mail->Password   = 'z0Th@n345';                            // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('tech.ecommerceeit@gmail.com');
			$mail->addAddress($owneremail);     						// Add a recipient
			

			// Content
			$mail->isHTML(true);                                  		// Set email format to HTML
			$mail->Subject = 'Application for Vendorship';
			$mail->Body    = '<p>Dear Applicant!</p><p>Your Application for Vendorship is Accepted.</p><p>Click on this Link and Login!</p><p>http://localhost/eit_ecom/management/vendor/login.php</p>Here are Your Login Credentials<br><br><b>Username:</b>'.$owneremail.'<br><b>Password:</b>'.$vendor_id.'</p><p>Login and Change Your Password.<br> Do not share this mail with anyone.<br>Contact Admin for any queries.</p>';
			

			if($mail->send()){
				if($con->query($sql) == TRUE){
					$_SESSION['success'] = "The Application is Accepted! Check vendors List";
					header("location: all_vendors.php");
					exit();
				}else{
					$_SESSION['error'] = "You Application did not get Accepted! Contact Developers";
					header("location: index.php");
					exit();
				}
			}else{
				$_SESSION['error'] = 'Mail Could not be Sent! The email was incorrect';
				header("location: index.php");
				exit();
			}
		}	
	}else{
		$_SESSION['error'] = 'Please Select an Application';
		header("location: view_application.php");
		exit();
	}
}else{
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: login.php");
	exit();
}
?>