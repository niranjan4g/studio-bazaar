<?php include 'header.php'; ?>

<?php
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are Unauthorized! Please Login';
	header("location: login.php");
	exit();
}else{
	if(isset($_GET['category_id'])){
		$category_id = $_GET['category_id'];
		$sql1 = "SELECT * FROM categories WHERE category_id = '$category_id'";
		$res = $con->query($sql1);
		if($res->num_rows == 0){
			$_SESSION['error'] = 'No Category Found!';
			header("location: all_categories.php");
			exit();
		}else{
			$rec = $res->fetch_assoc();
		}
	}else{
		$_SESSION['error'] = 'Please Select a Category';
		header("location: all_categories.php");
		exit();
	}
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update Category</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Update category</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Update Category</h3>
          </div>
          <!-- /.card-header -->
          <form role="form" action="" method="post" >
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
			   <div class="form-group">
                  <label>Category Name</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<input name="category_name" class="form-control" type="text" value="<?php echo $rec["category_name"]; ?>">
					
					
					
					</div>
				</div>
				 
			<div class="form-group">
                  <label>Status</label>
                <div class="input-group mb-3">
					<div class="input-group-prepend">
   
					</div>
					<?php if($rec['is_active'] == 1) { ?>
						<div class="input-group">
						  <div class="input-group-prepend">
							<div class="input-group-text">
							  <input type="radio" name="status" value="1" checked>
							</div>
						  </div>
						  <input type="text" class="form-control" value="ACTIVE" readonly>
						</div>
						
						<div class="input-group" style="margin-top: 5px;">
						  <div class="input-group-prepend">
							<div class="input-group-text">
							  <input type="radio" name="status" value="0">
							</div>
						  </div>
						  <input type="text" class="form-control" value="INACTIVE" readonly>
						</div>
					<?php } else { ?>
						<div class="input-group">
						  <div class="input-group-prepend">
							<div class="input-group-text">
							  <input type="radio" name="status" value="1">
							</div>
						  </div>
						  <input type="text" class="form-control" value="ACTIVE" readonly>
						</div>
						
						<div class="input-group" style="margin-top: 5px;">
						  <div class="input-group-prepend">
							<div class="input-group-text">
							  <input type="radio" name="status" value="0" checked>
							</div>
						  </div>
						  <input type="text" class="form-control" value="INACTIVE" readonly>
						</div>
					<?Php } ?>
					</div>
				</div>
				




                </div>
                <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">

              </div>
              <!-- /.form-group -->

              <!-- /.form-group -->
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-danger">Update</button>
            </div>
            </form>

            </div>
            <!-- /.row -->
          </div>
        </div>
       
                <!-- /.col-md-6 -->
              </div>
              <!-- /.row -->
            </div><!-- /.container-fluid -->
<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
	$category_name = $_POST["category_name"];
	$status = $_POST["status"];
	
	
	
	$sql = "UPDATE categories SET category_name = '$category_name', updated_by = 'Admin', is_active = '$status', updated_on = NOW() WHERE category_id = '$category_id'";
	if($con->query($sql) == TRUE){
		$_SESSION['success'] = 'Category Updated Successfully!';
		?>
		<script>
			window.open("all_categories.php","_self");
		</script>
		<?php
	}
	else {
   $_SESSION['error'] = 'Failed! Contact Developer';
		?>
		<script>
			window.open("all_categories.php","_self");
		</script>
		<?php
}}
?>
<?php include 'footer.php'; ?>
