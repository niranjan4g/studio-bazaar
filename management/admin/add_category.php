<?php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not authorized! Please Login!';
	header("location: login.php");
	exit();
}else{
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		
		$target_dir = "uploaded_files/category/"; //directory to store category images
		$target_file = basename($_FILES["category_file"]["name"]);
		$uploadOk = 1;
		
		if(empty($target_file)){
					$_SESSION['error'] = "Please Upload Category Image";
					header("location: all_categories.php");
					exit();
			}else{
				$random = rand(0,999999999);
				$name_new = ($random.$target_file);
				$imageFileType = strtolower(pathinfo($name_new,PATHINFO_EXTENSION));
					
				if ($_FILES["category_file"]["size"] > 500000) {
					$_SESSION['error'] = "Sorry, your file is too large.";
					$uploadOk = 0;
					header("location: all_categories.php");
					exit();
				}
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
					$_SESSION['error'] = "Sorry, only JPG, JPEG & PNG files are allowed.";
					$uploadOk = 0;
					header("location: all_categories.php");
					exit();
				}
				if ($uploadOk == 0) {
					$_SESSION['error'] = "Sorry, your Document was not uploaded! Contact Admin.";
					header("location: all_categories.php");
					exit();
				} else {
					if (move_uploaded_file($_FILES["category_file"]["tmp_name"], $target_dir.$name_new)) {
						$_SESSION['succcess'] = "Category Image was Successfully Uploaded";
					} else {
						$_SESSION['error'] = "Sorry, there was an error uploading your Category Image.";
						header("location: all_categories.php");
						exit();
					}
				}
				$category_file = $name_new;
			}
		
		$category_name = $_POST['category_name'];
		$n=6; 
			function getName($n) { 
				$characters = '0123456789'; 
				$randomString = 'STBCAT'; 
		  
				for ($i = 0; $i < $n; $i++) { 
					$index = rand(0, strlen($characters) - 1); 
					$randomString .= $characters[$index]; 
				} 
				return $randomString; 
			}
		$category_id = getName($n); 
		$sql = "INSERT INTO categories (category_id, category_name, category_cover, created_by, created_on) VALUES ('$category_id', '$category_name', '$category_file', 'Admin', NOW());";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Category Added Successfully!';
			header("location: all_categories.php");
			exit();
		}else{
			$_SESSION['error'] = 'Failed! Contact Developer';
			header("location: all_categories.php");
			exit();
		}
	}else{
		$_SESSION['error'] = 'Not Authorized';
		header("location: all_categories.php");
		exit();
	}
}
?>