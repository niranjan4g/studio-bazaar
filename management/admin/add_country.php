<?php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not authorized! Please Login!';
	header("location: login.php");
	exit();
}else{
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$country_name = $_POST['country_name'];
		$sql = "INSERT INTO countries (country_name) VALUES ('$country_name');";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'Country Added Successfully!';
			header("location: all_countries.php");
			exit();
		}else{
			$_SESSION['error'] = 'Failed! Contact Developer';
			header("location: all_countries.php");
			exit();
		}
	}else{
		$_SESSION['error'] = 'Not Authorized';
		header("location: all_countries.php");
		exit();
	}
}
?>