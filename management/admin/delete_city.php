<?Php
include '../db.php';
if(!isset($_SESSION['admin'])){
	$_SESSION['error'] = 'You are not Authorized! Please Login';
	header("location: all_cities.php");
	exit();
}else{
	if(isset($_GET['city_id'])){
		$city_id = $_GET['city_id'];
		$sql = "DELETE FROM cities WHERE city_id = '$city_id'";
		if($con->query($sql) == TRUE){
			$_SESSION['success'] = 'City Successfully Deleted';
			header("location: all_cities.php");
			exit();
		}else{
			$_SESSION['error'] = 'Delete Failed! Contact Developer';
			header("location: all_cities.php");
			exit();
		}
	}else{
		$_SESSION['error'] = "Please Select a City!";
		header("location: all_cities.php");
		exit();		
	}
}
?>