<?php include 'includes/header.php'; ?>
<?php
if(isset($_GET)){
	$user_id = htmlspecialchars(base64_decode(base64_decode(base64_decode($_GET['id']))));
	$link_url = htmlspecialchars($_GET['key']);
	$click_time = htmlspecialchars($_GET['code']);
	$current_time = time();
	$diff = $current_time - $click_time;
	if($diff < 4400){
		$result = $con->query("SELECT pwd_link FROM users WHERE user_id = '$user_id'");
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$pwd_link = $row['pwd_link'];
			
			if($pwd_link === $link_url){
				$qry = $con->query("UPDATE users SET pwd_link = '' WHERE user_id = '$user_id'");
			}else{
				
			?>
			<script>
			alert("Something Went Wrong! contact admin");
			window.open("index.php","_self");
			</script>
			<?php
			}
		}else{ 
		
		?>
			<script>
			alert("You are not authorized!");
			window.open("index.php","_self");
			</script>
		<?php }
	}else{
		
		?>
		<script>
		alert("Link has Expired! Try Again");
			window.open("forgot-password.php","_self");
			</script>
		<?php
	}
}
?>

<body>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Reset Password</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Reset Password</li>
                    </ul>
                </div>
            </div>
        </div>
		
        <div class="login-register-area pt-85 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-12 ml-auto mr-auto">
                        <div class="login-register-wrapper">
                            <div class="login-register-tab-list nav">
                                <a class="active" data-toggle="tab" href="#lg1">
                                    <h4> Reset Password </h4>
                                </a>
                               
                            </div>
							<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
                            <div class="tab-content">
                                <div id="lg1" class="tab-pane active">
                                    <div class="login-form-container">
                                        <div class="login-register-form">
                                            <form action="updatePassword.php" method="post">
                                                <input type="password" id="npassword" name="password" placeholder="Enter Password" required>
                                                <input type="password" id="cpassword" name="cpassword" placeholder="Confirm Password" required>
												<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
                                                <div class="button-box">
                                                   
                                                    <button id="changep" type="submit">Change Password</button>
                                                </div>
                                            </form>
											<p id="error-message"></p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include 'includes/footer.php'; ?>