<?php include 'includes/header.php'; ?>
<?php
if (!isset($_GET['page'])) {
					$page = 1;
					} else {
					$page = $_GET['page'];
					}
					
				?>
<body>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Our Services</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Our Services</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="shop-area pt-90 pb-90">
            <div class="container">
                <div class="row">
				<?php
				
				$count=1;
										$results_per_page = 12;
										
										if(isset($_GET['category_id'])){
											$categoryid = base64_decode(base64_decode(base64_decode($_GET['category_id'])));
											$sqli ="SELECT * FROM services WHERE category_id = '$categoryid' AND is_active = 1 ORDER BY availability DESC";
										}else{
											$sqli ="SELECT * FROM services WHERE is_active = 1 ORDER BY availability DESC";
										}
										$data = $con->query($sqli);
										$number_of_results = mysqli_num_rows($data);
										$number_of_pages = ceil($number_of_results/$results_per_page);

										
										$this_page_first_result = ($page-1)*$results_per_page;
										if(isset($_GET['category_id'])){
											$categoryid = base64_decode(base64_decode(base64_decode($_GET['category_id'])));
											$sql="SELECT * FROM services WHERE category_id = '$categoryid' AND is_active = 1 LIMIT " . $this_page_first_result . ',' .  $results_per_page;
										}else{
											$sql="SELECT * FROM services WHERE is_active = 1 LIMIT " . $this_page_first_result . ',' .  $results_per_page;
										}
										
										$result = $con->query($sql);
										if($number_of_results <= $results_per_page){
											$showing = $number_of_results;
										}else{
											$showing = $results_per_page;
										}
				?>
                    <div class="col-lg-12">
                        <div class="shop-topbar-wrapper">
                            <div class="shop-topbar-left">
                                <div class="view-mode nav">
                                    <a class="active" href="#shop-1" data-toggle="tab"><i class="la la-th"></i></a>
                                   
                                </div>
                                <p>Showing <?php if($showing == 0) { echo '0'; } else { echo '1'; } ?> - <?php echo $showing; ?> of <?php echo $number_of_results; ?> results </p>
                            </div>
                            <div class="product-sorting-wrapper">
                              
                                <div class="product-show shorting-style">
                                    <label>Sort by:</label>
                                    <div class="dropdown">
										  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: none; color: black; font-size: 13px;">
											All Categories
										  </button>
										  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										  <a class="dropdown-item" href="shop.php">All Services</a>
										  <?php 
										  $res = $con->query("SELECT * FROM categories WHERE is_active = 1");
										  while($rec = $res->fetch_assoc()){
										  ?>
											<a class="dropdown-item" href="shop.php?category_id=<?php echo base64_encode(base64_encode(base64_encode($rec['category_id']))); ?>"><?php echo $rec['category_name']; ?></a>
										  <?php } ?>
										  </div>
										</div>
                                </div>
                            </div>
                        </div>
                        <div class="shop-bottom-area">
                            <div class="tab-content jump">
                                <div id="shop-1" class="tab-pane active">
                                    <div class="row">
									<?php
										
										if($result->num_rows == 0){ ?>
											<center style="font-size: 20px; margin-left: 20px;">No Services Found!</center>
											
										<?php }else{ 
										while ($row = $result->fetch_assoc()) { ?>
											<?php
												$category_id = $row['category_id'];
												$rec = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
											
											?>
                                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                            <div class="product-wrap mb-35">
                                                <div class="product-img mb-15">
                                                    <a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($row['service_id']))); ?>"><img src="management/admin/uploaded_files/category/<?php echo $rec['category_cover']; ?>" alt="Service"></a>
                                                    <div class="product-action">
                                                        <a title="View" href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($row['service_id']))); ?>"><i class="la la-eye"></i></a>
														<?php if(isset($_SESSION['user'])){
															$user_id = $_SESSION['user'];
															$service_id = $row['service_id'];
															$ques = $con->query("SELECT * FROM wishlist WHERE user_id = '$user_id' AND service_id = '$service_id'");
															if($ques->num_rows == 0) {
															?>
                                                        <a title="Add to Wishlist" data-id="<?php echo $row['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart-o"></i></a>
															<?php } else { ?>
															
														<a title="Wishlisted" data-id="<?php echo $row['service_id']; ?>" class="add-wishlist" href="javascrtipt:void(0)"><i id="heart" class="la la-heart"></i></a>
															<?php } } else { ?>
														<a title="Login" href="auth.php"><i class="la la-user"></i></a>
														<?php } ?>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <span><?php echo $rec['category_name']; ?></span>
                                                    <h4><a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($row['service_id']))); ?>"><?php echo $row['service_name']; ?></a></h4>
                                                    <div class="price-addtocart">
                                                        <div class="product-price">
														<?php
										$discount = $row['discount_amount'];
										$price = $row['service_price'];
										$discount_type = $row['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
                                                            <span>₹<?php echo $discount_price; ?></span>
                                                        </div>
                                                        <div class="product-addtocart">
								<?php if(isset($_SESSION['user'])) { 
								if($row['availability'] == 0) { 
								$user_id = $_SESSION['user'];
									$service_id = $row['service_id'];
									$resp = $con->query("SELECT * FROM cart WHERE user_id = '$user_id' AND service_id = '$service_id'");
									if($resp->num_rows == 0) {
								?>
                                    <a title="Add To Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">+ Add To Cart</a>
									<?php } else { ?>
									<a title="Already in Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">+ Added In Cart</a>
								<?php } } else { ?> 
									 <a title="Temporarily Unavailable">Unavailable</a>
								<?php } } else { ?>
									<a title="Login" href="auth.php">+ Login to Add to Cart</a>
								<?php } ?>
                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										<?php } } ?>
                                        
                                        
                                    </div>
                                </div>
                                
                                <div class="pagination-style text-center">
								<?php if($showing == 0) { ?>
								
								
								<?php
								} else if(!isset($_GET['category_id'])) {
								?>
                                    <ul>
									<?php
									   $pageid = $page;
									   if($pageid != 1)
									   {
									   ?>
                                        <li><a class="prev" href="?page=<?php echo $pageid-1; ?>"><i class="la la-angle-left"></i></a></li>
										<?php } else { ?>
   
										   <?php }?>
											<?php
											for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
											?>
                                        <li><a href="?page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
                                        <?php } ?>
											<?php
										   $pageid = $page;
										   $pagei = $pagei - 1;
										   if($pageid != $pagei){
										   ?>
                                        <li><a class="next" href="?page=<?php echo $pageid+1; ?>"><i class="la la-angle-right"></i></a></li>
										<?php } else { ?>
   
										<?php } ?>
                                    </ul>
								<?php } else { ?>
								<ul>
									<?php
									   $pageid = $page;
									   if($pageid != 1)
									   {
									   ?>
                                        <li><a class="prev" href="?category_id=<?php echo base64_encode(base64_encode(base64_encode($categoryid))); ?>&page=<?php echo $pageid-1; ?>"><i class="la la-angle-left"></i></a></li>
										<?php } else { ?>
   
										   <?php }?>
											<?php
											for ($pagei=1;$pagei<=$number_of_pages;$pagei++) {
											?>
                                        <li><a href="?category_id=<?php echo base64_encode(base64_encode(base64_encode($categoryid))); ?>&page=<?php echo $pagei; ?>"><?php echo $pagei; ?></a></li>
                                        <?php } ?>
											<?php
										   $pageid = $page;
										   $pagei = $pagei - 1;
										   if($pageid != $pagei){
										   ?>
                                        <li><a class="next" href="?category_id=<?php echo base64_encode(base64_encode(base64_encode($categoryid))); ?>&page=<?php echo $pageid+1; ?>"><i class="la la-angle-right"></i></a></li>
										<?php } else { ?>
   
										<?php } ?>
                                    </ul>
								<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <?php include 'includes/footer.php'; ?>