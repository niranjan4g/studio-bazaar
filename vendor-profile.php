<?php include 'includes/header.php'; ?>
<?php
if(!isset($_GET['vendor_id'])){
	$_SESSION['error'] = 'Select a Vendor to View';
	header("location: vendors.php");
	exit();
}else{
	$vendor_id = base64_decode(base64_decode(base64_decode($_GET['vendor_id'])));
	//echo $vendor_id;
	$res = $con->query("SELECT * FROM vendors WHERE vendor_id = '$vendor_id'");
	if($res->num_rows == 0){
		$_SESSION['error'] = 'Vendor does not exist';
		header("location: vendors.php");
		exit();
	}else{
		$record = $res->fetch_assoc();
	}
}
?>
<body>
  <style>
  body{
    color: #6F8BA4;
}
.section {
    padding: 100px 0;
    position: relative;
}
.gray-bg {
    background-color: #ffffff;
}
img {
    max-width: 100%;
}
img {
    vertical-align: middle;
    border-style: none;
}
/* About Me 
---------------------*/
.about-text h3 {
  font-size: 45px;
  font-weight: 700;
  margin: 0 0 6px;
}
@media (max-width: 767px) {
  .about-text h3 {
    font-size: 35px;
  }
}
.about-text h6 {
  font-weight: 600;
  margin-bottom: 15px;
}
@media (max-width: 767px) {
  .about-text h6 {
    font-size: 18px;
  }
}
.about-text p {
  font-size: 18px;
  max-width: 450px;
}
.about-text p mark {
  font-weight: 600;
  color: #20247b;
}

.about-list {
  padding-top: 10px;
}
.about-list .media {
  padding: 5px 0;
}
.about-list label {
  color: #20247b;
  font-weight: 600;
  width: 88px;
  margin: 0;
  position: relative;
}
.about-list label:after {
  content: "";
  position: absolute;
  top: 0;
  bottom: 0;
  right: 11px;
  width: 1px;
  height: 12px;
  background: #20247b;
  -moz-transform: rotate(15deg);
  -o-transform: rotate(15deg);
  -ms-transform: rotate(15deg);
  -webkit-transform: rotate(15deg);
  transform: rotate(15deg);
  margin: auto;
  opacity: 0.5;
}
.about-list p {
  margin: 0;
  font-size: 15px;
}

@media (max-width: 991px) {
  .about-avatar {
    margin-top: 30px;
  }
}

.about-section .counter {
  padding: 22px 20px;
  background: #ffffff;
  border-radius: 10px;
  box-shadow: 0 0 30px rgba(31, 45, 61, 0.125);
}
.about-section .counter .count-data {
  margin-top: 10px;
  margin-bottom: 10px;
}
.about-section .counter .count {
  font-weight: 700;
  color: #20247b;
  margin: 0 0 5px;
}
.about-section .counter p {
  font-weight: 600;
  margin: 0;
}
mark {
    background-image: linear-gradient(rgba(252, 83, 86, 0.6), rgba(252, 83, 86, 0.6));
    background-size: 100% 3px;
    background-repeat: no-repeat;
    background-position: 0 bottom;
    background-color: transparent;
    padding: 0;
    color: currentColor;
}
.theme-color {
    color: #fc5356;
}
.dark-color {
    color: #20247b;
}


  </style>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Studio Profile</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Studio Profile</li>
                    </ul>
                </div>
            </div>
        </div>
		
		<section class="section about-section gray-bg" id="about">
            <div class="container">
                <div class="row align-items-center flex-row-reverse">
                    <div class="col-lg-9">
                        <div class="about-text go-to">
                            <h3 class="dark-color">About <?php echo $record['bussinessname']; ?></h3>
                            <h6 class="theme-color lead">VENDOR ID: <?php echo $record['vendor_id']; ?></h6>
							 <h6 class="dark-color lead">OWNER NAME: <?php echo $record['ownername']; ?></h6>
                            <p><mark>Address</mark>: <?php echo $record['bussinessaddress']; ?></p>
                            <div class="row about-list">
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>Contact</label>
                                        <p><?php echo $record['bussinessphone']; ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Email</label>
                                        <p><?php echo $record['owneremail']; ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Map Link</label>
                                        <p><?php echo $record['mapaddress']; ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Joined</label>
                                        <p><?php echo date('d F, Y', strtotime($record['verified_on'])); ?></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>Facebook</label>
                                        <p><?php if($record['facebook'] == NULL) { echo 'Not Added'; } else { echo $record['facebook']; } ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Twitter</label>
                                        <p><?php if($record['twitter'] == NULL) { echo 'Not Added'; } else { echo $record['twitter']; } ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Instagram</label>
                                        <p><?php if($record['instagram'] == NULL) { echo 'Not Added'; } else { echo $record['instagram']; } ?></p>
                                    </div>
                                    <div class="media">
                                        <label>Youtube</label>
                                        <p><?php if($record['youtube'] == NULL) { echo 'Not Added'; } else { echo $record['youtube']; } ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="about-avatar">
                            <img src="uploaded_files/bussinesslogo/<?php echo $record['bussinesslogo']; ?>" title="" style="border-radius: 50%;" alt="">
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
		
        <div class="shop-area pt-90 pb-90">
            <div class="container">
                <div class="row">
				
                    <div class="col-lg-12">
                        <div class="shop-topbar-wrapper">
                            <div class="shop-topbar-left">
                                <div class="view-mode nav">
                                    <a class="active" href="#shop-1" data-toggle="tab"><i class="la la-th"></i></a>
                                   
                                </div>
                                <p>Services By <?php echo $record['bussinessname']; ?></p>
                            </div>
                           
                        </div>
                        <div class="shop-bottom-area">
                            <div class="tab-content jump">
                                <div id="shop-1" class="tab-pane active">
                                    <div class="row">
									<?php
										$sql = "SELECT * FROM services WHERE vendor_id = '$vendor_id'";
										$result = $con->query($sql);
										if($result->num_rows == 0){ ?>
											<center style="font-size: 20px; margin-left: 20px;">No Services Found!</center>
											
										<?php }else{ 
										while ($row = $result->fetch_assoc()) { ?>
											<?php
												$category_id = $row['category_id'];
												$rec = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
											
											?>
                                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                                            <div class="product-wrap mb-35">
                                                <div class="product-img mb-15">
                                                    <a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($row['service_id']))); ?>"><img src="management/admin/uploaded_files/category/<?php echo $rec['category_cover']; ?>" alt="Service"></a>
                                                    <div class="product-action">
                                                        <a title="View" href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($row['service_id']))); ?>"><i class="la la-eye"></i></a>
														<?php if(isset($_SESSION['user'])){
															$user_id = $_SESSION['user'];
															$service_id = $row['service_id'];
															$ques = $con->query("SELECT * FROM wishlist WHERE user_id = '$user_id' AND service_id = '$service_id'");
															if($ques->num_rows == 0) {
															?>
                                                        <a title="Add to Wishlist" data-id="<?php echo $row['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart-o"></i></a>
															<?php } else { ?>
															
														<a title="Wishlisted" data-id="<?php echo $row['service_id']; ?>" class="add-wishlist" href="javascrtipt:void(0)"><i id="heart" class="la la-heart"></i></a>
															<?php } } else { ?>
														<a title="Login" href="auth.php"><i class="la la-user"></i></a>
														<?php } ?>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <span><?php echo $rec['category_name']; ?></span>
                                                    <h4><a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($row['service_id']))); ?>"><?php echo $row['service_name']; ?></a></h4>
                                                    <div class="price-addtocart">
                                                        <div class="product-price">
														<?php
										$discount = $row['discount_amount'];
										$price = $row['service_price'];
										$discount_type = $row['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
                                                            <span>₹<?php echo $discount_price; ?></span>
                                                        </div>
                                                         <div class="product-addtocart">
								<?php if(isset($_SESSION['user'])) { 
								if($row['availability'] == 0) { 
								$user_id = $_SESSION['user'];
									$service_id = $row['service_id'];
									$resp = $con->query("SELECT * FROM cart WHERE user_id = '$user_id' AND service_id = '$service_id'");
									if($resp->num_rows == 0) {
								?>
                                    <a title="Add To Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">+ Add To Cart</a>
									<?php } else { ?>
									<a title="Already in Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">+ Added In Cart</a>
								<?php } } else { ?> 
									 <a title="Temporarily Unavailable">Unavailable</a>
								<?php } } else { ?>
									<a title="Login" href="auth.php">+ Login to Add to Cart</a>
								<?php } ?>
                                </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										
										<?php } } ?>
                                        
                                        
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   <?php include 'includes/footer.php'; ?>