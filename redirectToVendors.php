<?php
include 'includes/config.php';
if($_SERVER['REQUEST_METHOD'] == "POST"){
	$country = base64_encode(base64_encode(base64_encode($_POST['country'])));
	$state = base64_encode(base64_encode(base64_encode($_POST['state'])));
	$city = base64_encode(base64_encode(base64_encode($_POST['city'])));
	
	if($country == NULL && $state == NULL && $city == NULL){
		header("location: vendors.php");
		exit();
	}
	if($state == NULL && $city == NULL){
		$token = http_build_query(array('country'=>$country));
		header("location: vendors.php?$token");
		exit();
	}
	if($city == NULL){
		$token = http_build_query(array('country'=>$country,'state'=>$state));
		header("location: vendors.php?$token");
		exit();
	}
	if($country != NULL && $state != NULL && $city != NULL){
		$token = http_build_query(array('country'=>$country,'state'=>$state,'city'=>$city));
		header("location: vendors.php?$token");
		exit();
	}
	
}else{
	$_SESSION['error'] = "Forbidden";
	header("location: index.php");
	exit();
}
?>