<?php
include 'includes/config.php'; 
if(isset($_SESSION['user'])){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$oldpassword = $_POST["oldpassword"];
		$password = $_POST["password"];
		$cpassword = $_POST["cpassword"];
		
		$user_id = $_SESSION['user'];
		$sql = "SELECT password FROM users WHERE user_id = '$user_id'";
		//$hashpassword = password_hash($oldpassword, PASSWORD_DEFAULT);
		$result = $con->query($sql);
		$record = $result->fetch_assoc();
		$fetchedpassword = $record["password"];
		
		if(password_verify($oldpassword, $fetchedpassword)){
			if($password == $cpassword){
				$hashedpassword = password_hash($password, PASSWORD_DEFAULT);
				$query = "UPDATE users SET password = '$hashedpassword' WHERE user_id = '$user_id'";
				if($con->query($query) == TRUE){
					$_SESSION['success'] = "Password Updated Successfully!";
					header("location: myaccount.php");
				}else{
					$_SESSION['error'] = "Password wasn't updated! Contact Admin";
					header("location: myaccount.php");
				}
			}else{
				$_SESSION['error'] = "Passwords do not Match!";
				header("location: myaccount.php");
			}
		}else{
			$_SESSION['error'] = "Enter Current Password Correctly!";
			header("location: myaccount.php");
		}
	}
}else{
	$_SESSION['error'] = "You are unauthorized! Please Login";
	header("location: index.php");
}
?>