<?php include 'includes/header.php'; ?>
<?php
if(!isset($_SESSION['user'])){
	$_SESSION['error'] = "You are not Loggen In! Login First";
	header("location: index.php");
}else{
	$user_id = $_SESSION['user'];
}

?>
<body>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>My Account</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">My account </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- my account wrapper start -->
        <div class="my-account-wrapper pt-100 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- My Account Page Start -->
                        <div class="myaccount-page-wrapper">
                            <!-- My Account Tab Menu Start -->
                            <div class="row">
                                <div class="col-lg-3 col-md-4">
                                    <div class="myaccount-tab-menu nav" role="tablist">
                                        <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i>
                                            Dashboard</a>
                                        <a href="#" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Orders</a>
                                        <a href="#download" data-toggle="tab"><i class="fa fa-cloud-download"></i> Edit Profile</a>
                                        <a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Change Password</a>
                                       
                                       
                                        <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a>
                                    </div>
                                </div>
                                <!-- My Account Tab Menu End -->
                                <!-- My Account Tab Content Start -->
                                <div class="col-lg-9 col-md-8">
                                    <div class="tab-content" id="myaccountContent">
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Dashboard</h3>
                                                <div class="welcome">
                                                    <p>Hello, <strong><?php echo $sesdata["name"]; ?></strong></p>
                                                </div>

                                                <p class="mb-0">Email:&nbsp;&nbsp;<?php echo $sesdata["email"]; ?></p>
												<p class="mb-0">Phone Number:&nbsp;&nbsp;<?php echo $sesdata["phone"]; ?></p>
												<hr>
												<div class="row">
													<div class="col">
														<div class="card" style="width: 10rem;">
														  <div class="card-body">
															<h5 class="card-title">Total Orders Placed</h5>
															<p class="card-text">0</p>
															
														  </div>
														</div>
													</div>
													<div class="col">
														<div class="card" style="width: 10rem;">
														  <div class="card-body">
														  <?php
														  $rec = $con->query("SELECT COUNT(*) as total_wish FROM wishlist WHERE user_id = '$user_id'")->fetch_assoc();
														  ?>
															<h5 class="card-title">Total Products in Wishlist<a href="wishlist.php">&nbsp;<i class="la la-arrow-right" style="font-size: 18px; font-weight: bold;"></i></a></h5>
															<p class="card-text"><?php echo $rec['total_wish']; ?></p>
															
														  </div>
														</div>
													</div>
													<div class="col">
														<div class="card" style="width: 10rem;">
														  <div class="card-body">
															<h5 class="card-title">Total Products in Cart<a href="cart.php">&nbsp;<i class="la la-arrow-right" style="font-size: 18px; font-weight: bold;"></i></a></h5>
															<p class="card-text">0</p>
															
														  </div>
														</div>
													</div>
												</div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="orders" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Orders</h3>
                                                <div class="myaccount-table table-responsive text-center">
                                                    <table class="table table-bordered">
                                                        <thead class="thead-light">
                                                            <tr>
                                                                <th>Order</th>
                                                                <th>Date</th>
                                                                <th>Status</th>
                                                                <th>Total</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Aug 22, 2018</td>
                                                                <td>Pending</td>
                                                                <td>$3000</td>
                                                                <td><a href="cart.html" class="check-btn sqr-btn ">View</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>July 22, 2018</td>
                                                                <td>Approved</td>
                                                                <td>$200</td>
                                                                <td><a href="cart.html" class="check-btn sqr-btn ">View</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td>June 12, 2017</td>
                                                                <td>On Hold</td>
                                                                <td>$990</td>
                                                                <td><a href="cart.html" class="check-btn sqr-btn ">View</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="download" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Edit Profile</h3>
                                                <div class="account-details-form">
                                                    <form action="update_user.php" method="post">
                                                         <div class="single-input-item">
                                                            <label for="display-name" class="required">Name</label>
                                                            <input type="text" id="display-name" name="name" value="<?Php echo$sesdata["name"]; ?>" required />
                                                        </div>
                                                        <div class="single-input-item">
                                                            <label for="display-phone" class="required">Phone</label>
                                                            <input type="text" id="display-phone" name="phone" value="<?php echo $sesdata["phone"]; ?>" required />
                                                        </div>
                                                        <div class="single-input-item">
                                                            <label for="email" class="required">Email Addres</label>
                                                            <input type="email" id="email" name="email" value="<?Php echo $sesdata["email"]; ?>" required />
                                                        </div>
                                                       
                                                        <div class="single-input-item">
                                                            <button class="check-btn sqr-btn ">Update Profile</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->
                                        <!-- Single Tab Content Start -->
                                        <div class="tab-pane fade" id="payment-method" role="tabpanel">
                                            <div class="myaccount-content">
                                                <h3>Change Password</h3>
                                                <div class="account-details-form">
                                                    <form action="change_password.php" method="POST">
                                                      
                                                        <fieldset>
                                                            
                                                            <div class="single-input-item">
                                                                <label for="current-pwd" class="required">Current Password</label>
                                                                <input type="password" id="current-pwd" name="oldpassword" required />
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="new-pwd" class="required">New Password</label>
                                                                        <input type="password" id="new-pwd" name="password" required />
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="single-input-item">
                                                                        <label for="confirm-pwd" class="required">Confirm Password</label>
                                                                        <input type="password" id="confirm-pwd" name="cpassword" required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div class="single-input-item">
                                                            <button class="check-btn sqr-btn ">Change Password</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Single Tab Content End -->
                                        
                                        <!-- Single Tab Content End -->
                                        
                                    </div>
                                </div> <!-- My Account Tab Content End -->
                            </div>
                        </div> <!-- My Account Page End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- my account wrapper end -->
      <?php include 'includes/footer.php'; ?>