<?php
include 'includes/config.php'; 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

if(isset($_SESSION['user'])){
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		$owneremail = $_POST['owneremail'];
		$result = $con->query("SELECT * FROM vendors WHERE owneremail = '$owneremail' AND status = 0 OR status = 2");
		if($result->num_rows > 0){
			$_SESSION['error'] = "You have already Applied for Vendorship";
			header("location: index.php");
			exit();
		}else{
			$target_dir1 = "uploaded_files/aadharcard/"; //directory to store aadhar card
			$target_dir2 = "uploaded_files/pancard/";	//directory to store pancard
			$target_dir3 = "uploaded_files/bussinesslogo/"; //directory to store logo
			
			$target_file1 = basename($_FILES["aadharimage"]["name"]);
			$target_file2 = basename($_FILES["pancardimage"]["name"]);
			$target_file3 = basename($_FILES["bussinesslogo"]["name"]);
			$uploadOk = 1;
			
			if(empty($target_file1)){
					$_SESSION['error'] = "Please Uplaod your Aadhar Card";
					header("location: apply_vendor.php");
					exit();
			}else{
				$random1 = rand(0,999999999);
				$name_new1 = ($random1.$target_file1);
				$imageFileType1 = strtolower(pathinfo($name_new1,PATHINFO_EXTENSION));
					
				if ($_FILES["aadharimage"]["size"] > 500000) {
					$_SESSION['error'] = "Sorry, your file is too large.";
					$uploadOk = 0;
					header("location: apply_vendor.php");
					exit();
				}
				if($imageFileType1 != "jpg" && $imageFileType1 != "png" && $imageFileType1 != "jpeg") {
					$_SESSION['error'] = "Sorry, only JPG, JPEG & PNG files are allowed.";
					$uploadOk = 0;
					header("location: apply_vendor.php");
					exit();
				}
				if ($uploadOk == 0) {
					$_SESSION['error'] = "Sorry, your Document was not uploaded! Contact Admin.";
					header("location: apply_vendor.php");
					exit();
				} else {
					if (move_uploaded_file($_FILES["aadharimage"]["tmp_name"], $target_dir1.$name_new1)) {
						$_SESSION['succcess'] = "Aadhar Card was Successfully Uploaded";
					} else {
						$_SESSION['error'] = "Sorry, there was an error uploading your Aadhar Card.";
						header("location: apply_vendor.php");
						exit();
					}
				}
				$aadharimage = $name_new1;
			}
			if(empty($target_file2)){
					$_SESSION['error'] = "Please Upload your Pan Card";
					header("location: apply_vendor.php");
					exit();
			}else{
				$random2 = rand(0,999999999);
				$name_new2 = ($random2.$target_file2);
				$imageFileType2 = strtolower(pathinfo($name_new2,PATHINFO_EXTENSION));
					
				if ($_FILES["pancardimage"]["size"] > 500000) {
					$_SESSION['error'] = "Sorry, your file is too large.";
					$uploadOk = 0;
					header("location: apply_vendor.php");
					exit();
				}
				if($imageFileType2 != "jpg" && $imageFileType2 != "png" && $imageFileType2 != "jpeg") {
					$_SESSION['error'] = "Sorry, only JPG, JPEG & PNG files are allowed.";
					$uploadOk = 0;
					header("location: apply_vendor.php");
					exit();
				}
				if ($uploadOk == 0) {
					$_SESSION['error'] = "Sorry, your Document was not uploaded! Contact Admin.";
					header("location: apply_vendor.php");
				} else {
					if (move_uploaded_file($_FILES["pancardimage"]["tmp_name"], $target_dir2.$name_new2)) {
						$_SESSION['succcess'] = "Pan Card was Successfully Uploaded";
					} else {
						$_SESSION['error'] = "Sorry, there was an error uploading your file.";
						header("location: apply_vendor.php");
						exit();
					}
				}
				$pancardimage = $name_new2;
			}
			if(empty($target_file3)){
					$_SESSION['error'] = "Please Upload your Bussiness Logo";
					header("location: apply_vendor.php");
					exit();
			}else{
				$random3 = rand(0,999999999);
				$name_new3 = ($random3.$target_file3);
				$imageFileType3 = strtolower(pathinfo($name_new3,PATHINFO_EXTENSION));
					
				if ($_FILES["bussinesslogo"]["size"] > 500000) {
					$_SESSION['error'] = "Sorry, your file is too large.";
					$uploadOk = 0;
					header("location: apply_vendor.php");
					exit();
				}
				if($imageFileType3 != "jpg" && $imageFileType3 != "png" && $imageFileType3 != "jpeg") {
					$_SESSION['error'] = "Sorry, only JPG, JPEG & PNG files are allowed.";
					$uploadOk = 0;
					header("location: apply_vendor.php");
					exit();
				}
				if ($uploadOk == 0) {
					$_SESSION['error'] = "Sorry, your Document was not uploaded! Contact Admin.";
					header("location: apply_vendor.php");
					exit();
				} else {
					if (move_uploaded_file($_FILES["bussinesslogo"]["tmp_name"], $target_dir3.$name_new3)) {
						$_SESSION['succcess'] = "Bussiness logo was Successfully Uploaded";
					} else {
						$_SESSION['error'] = "Sorry, there was an error uploading your file.";
						header("location: apply_vendor.php");
						exit();
					}
				}
				$bussinesslogo = $name_new3;
			}
			$ownername = $_POST['ownername'];
			$ownerphone = $_POST['ownerphone'];
			$bussinessname = $_POST['bussinessname'];
			$bussinessnumber = $_POST['bussinessnumber'];
			$mapaddress = $_POST['mapaddress'];
			$bussinessaddress = $_POST['bussinessaddress'];
			$aadhar = $_POST['aadhar'];
			$pancard = $_POST['pancard'];
			$establishment = $_POST['establishmentnumber'];
			$gstinnumber = $_POST['gstinnumber'];
			$bankname = $_POST['bankname'];
			$accountnumber = $_POST['accountnumber'];
			$ifsccode = $_POST['ifsccode'];
			$branch = $_POST['branch'];
			$user = $_SESSION['user'];
			$country = $_POST['country'];
			$state = $_POST['state'];
			$city = $_POST['city'];
			
			$sql = "INSERT INTO vendors (bussinessname, bussinessaddress, bussinessphone, mapaddress, ownername, ownernumber, owneremail, pancard, aadhar, establishmentnumber, gstinumber, accountnumber, bankname, ifsccode, branch, aadharimage, pancardimage, bussinesslogo, country, state, city, created_on, created_by) VALUES ('$bussinessname','$bussinessaddress','$bussinessnumber','$mapaddress','$ownername', '$ownerphone', '$owneremail','$pancard','$aadhar','$establishment','$gstinnumber','$accountnumber', '$bankname', '$ifsccode', '$branch', '$aadharimage', '$pancardimage', '$bussinesslogo', '$country', '$state', '$city',  NOW(), '$user');";
			
			//code to send mail and execute query
			$mail = new PHPMailer(true);
			
			//Server settings
			$mail->SMTPDebug = 2;                                       // Enable verbose debug output
			$mail->isSMTP();                                            // Set mailer to use SMTP
			$mail->Host       = 'smtp.gmail.com';  						// Specify main and backup SMTP servers
			$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
			$mail->Username   = 'tech.ecommerceeit@gmail.com';               // SMTP username
			$mail->Password   = 'z0Th@n345';                            // SMTP password
			$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
			$mail->Port       = 587;                                    // TCP port to connect to

			//Recipients
			$mail->setFrom('tech.ecommerceeit@gmail.com');
			$mail->addAddress($owneremail);     						// Add a recipient
			

			// Content
			$mail->isHTML(true);                                  		// Set email format to HTML
			$mail->Subject = 'Application for Vendorship';
			$mail->Body    = '<h2>Thanks for Applying for Vendorship!</h2><br><p>Your Application for Vendorship is Submitted. Our Support Team will contact you soon!<br>For any Queries reply to this mail!';
			//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

			if($mail->send()){
				if($con->query($sql) == TRUE){
					$_SESSION['success'] = "Your Application is Submitted! Check confirmation email";
					header("location: index.php");
					exit();
				}else{
					$_SESSION['error'] = "You Application did not get submitted! Contact Admin";
					header("location: apply_vendor.php");
					exit();
				}
			}else{
				$_SESSION['error'] = 'We could not send you the Confirmation email. Please Enter correct email Id or contact Admin';
				header("location: index.php");
				exit();
			}			
		}
	}
}else{
	$_SESSION['error'] = "You are not Authorized! please Login";
	header("location: index.php");
	exit();
}
?>