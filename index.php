<?php include 'includes/header.php'; ?>
<body>
    <div class="main-wrapper">
        <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
	  <!-- Slider Start -->
        <div class="slider-area pt-20">
            <div class="container">
                <div class="slider-active-4 owl-carousel dot-style-2">
                    <div class="slider-height-8 bg-img res-white-overly-xs" style="background-image:url(assets/images/slider/slider.jpg);">
                        <div class="row align-items-center">
                            <div class="ml-auto col-lg-9 col-md-12 col-12 col-sm-12">
                                <div class="slider-content-8 slider-animated-1">
                                    <h1 class="animated">Welcome to Studio Bazaar</h1>
                                    <p class="animated">Live every moment of your life and get it Captured with us.</p>
                                    <div class="slider-btn-8 default-btn btn-hover hover-border-none">
                                        <a class="animated black-color" href="shop.php">EXPLORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-height-8 bg-img res-white-overly-xs" style="background-image:url(assets/images/slider/slider2.jpg);">
                        <div class="row align-items-center">
                            <div class="ml-auto col-lg-9 col-md-12 col-12 col-sm-12">
                                <div class="slider-content-8 slider-animated-1">
                                    <h1 class="animated">Find your Studio at your Finger Tips</h1>
                                    <p class="animated">You are just one click away from finding Awesome Photo Studios.</p>
                                    <div class="slider-btn-8 default-btn btn-hover hover-border-none">
                                        <a class="animated black-color" href="vendors.php">EXPLORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-height-8 bg-img res-white-overly-xs" style="background-image:url(assets/images/slider/slider1.jpg);">
                        <div class="row align-items-center">
                            <div class="ml-auto col-lg-9 col-md-12 col-12 col-sm-12">
                                <div class="slider-content-8 slider-animated-1">
                                    <h1 class="animated">Get your Photo From the Best Photographers in the Market.</h1>
                                    <p class="animated">Studio Bazaar brings you the most popular and trusted Studios.</p>
                                    <div class="slider-btn-8 default-btn btn-hover hover-border-none">
                                        <a class="animated black-color" href="vendors.php">EXPLORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	<!-- Slider End -->
        <div class="banner-area pt-30 pb-40">
            <div class="container">
			<div class="section-title-tab-wrap">
                    <div class="row">
                        <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4">
                            <div class="section-title-5">
                                <h2>Search Vendors in your Location</h2>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-8">
                           
                        </div>
                    </div>
                </div>
               <form action="redirectToVendors.php" method="post">
				  <div class="row">
					<div class="col">
					  <select class="form-control" name="country" id="country">
						<option value="">Select your Country</option>
						<?php
						$sql = $con->query("SELECT * FROM countries WHERE status=1");
						while($rec = $sql->fetch_assoc()){
						?>
						<option value="<?php echo $rec['country_id']; ?>"><?Php echo $rec['country_name']; ?></option>
						<?php } ?>
					  </select>
					</div>
					<div class="col">
					    <select class="form-control" name="state" id="state">
						<option value="">Select your State</option>
					  </select>
					</div>
					<div class="col">
					    <select class="form-control" name="city" id="city">
						<option value="">Select your City</option>
					  </select>
					</div>
					<div class="col">
					  <input type="submit" class="btn btn-success form-control" value="Search">
					</div>
				  </div>
				</form>
            </div>
        </div>
		
		
        <div class="product-area pb-40">
            <div class="container">
                <div class="section-title-tab-wrap">
                    <div class="row">
                        <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4">
                            <div class="section-title-5">
                                <h2>Featured Categories</h2>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-8">
                           
                        </div>
                    </div>
                </div>
                <div class="tab-content jump">
                    <div id="product-10" class="tab-pane active">
                        <div class="row">
						<?php
						$result = $con->query("SELECT * FROM categories WHERE is_active = 1 LIMIT 8");
						if($result->num_rows == 0){
							echo '<h3 style="margin-left: 2%;">No Categories Found!</h3>';
						}else{
							while($row = $result->fetch_assoc()){
						?>
						
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="product-wrap product-border-3 product-img-zoom mb-30">
                                    <div class="product-img">
                                        <a href="shop.php?category_id=<?php echo base64_encode(base64_encode(base64_encode($row['category_id']))); ?>"><img src="management/admin/uploaded_files/category/<?php echo $row['category_cover']; ?>" alt="Category"></a>
                                        <div class="product-action-4">
                                          
                                        </div>
                                    </div>
                                    <div class="product-content product-content-padding">
                                        <h4><a href="shop.php?category_id=<?php echo base64_encode(base64_encode(base64_encode($row['category_id']))); ?>"><?php echo $row['category_name']; ?></a></h4>
                                        <div class="price-addtocart">
                                            <div class="product-price">
                                                <a href="shop.php?category_id=<?php echo base64_encode(base64_encode(base64_encode($row['category_id']))); ?>" class="btn btn-info" style="font-size: 13px;">Explore</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
						<?php } } ?>
							
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
		
		
		
		
		
		
        <div class="banner-area pb-40">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="banner-wrap mb-30">
                            <a href="product-details.html"><img src="assets/images/banner/banner.jpg" alt="banner"></a>
                            <div class="banner-content-10">
                                <h2>Wedding Photography</h2>
                            </div>
                            <div class="banner-content-10-btn">
                                <a href="shop.php?category_id=VlRGU1ExRXdSbFZQUkdjeVRWUlZNQT09">Explore</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8">
                        <div class="banner-wrap mb-30 res-white-overly-xs res-white-overly-md">
                            <a href="product-details.html"><img src="assets/images/banner/banner1.jpg" alt="banner"></a>
                            <div class="banner-content-13">
                                <h3><span style="font-size: 45px;">The Wedding Season is On!!!</span></h3>
                                <p>Checkout the best Budget Friendly Wedding Photographers in your area and Capture your Moments.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
        <div class="product-area pb-40">
            <div class="container">
                <div class="section-title-tab-wrap">
                    <div class="row">
                        <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4">
                            <div class="section-title-5">
                                <h2>Recenty Added Services</h2>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-8">
                           
                        </div>
                    </div>
                </div>
                <div class="tab-content jump">
                    <div id="product-10" class="tab-pane active">
                        <div class="row">
						<?php
						$res = $con->query("SELECT * FROM services WHERE is_active = 1 AND availability = 0 ORDER BY serviceid DESC LIMIT 8");
						if($res->num_rows == 0){
							echo '<h3 style="margin-left: 2%;">No Services Found!</h3>';
						}else{
							while($array = $res->fetch_assoc()){
								$category_id = $array['category_id'];
								$que = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
						?>
						
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="product-wrap product-border-3 product-img-zoom mb-30">
                                    <div class="product-img">
                                        <a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($array['service_id']))); ?>"><img src="management/admin/uploaded_files/category/<?php echo $que['category_cover']; ?>" alt="service"></a>
                                        <div class="product-action-4">
                                            <div class="product-action-4-style">
											<?php if(isset($_SESSION['user'])) {
													if($array['availability'] == 0) {
														$user_id = $_SESSION['user'];
														$service_id = $array['service_id'];
														$ren = $con->query("SELECT * FROM cart WHERE user_id = '$user_id' AND service_id = '$service_id'");
														if($ren->num_rows == 0) {
												?>
                                                <a data-tooltip="Add To Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $array['service_id'];?>" data-vendor-id="<?php echo $array['vendor_id']; ?>"><i class="la la-cart-plus"></i></a>
														<?php } else { ?>
												<a data-tooltip="Added To Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $array['service_id'];?>" data-vendor-id="<?php echo $array['vendor_id']; ?>"><i class="la la-shopping-cart"></i></a>
												<?php
														}
													$rer = $con->query("SELECT * FROM wishlist WHERE user_id = '$user_id' AND service_id = '$service_id'");
													if($rer->num_rows == 0) {
												?>
                                                <a data-tooltip="Add to Wishlist" data-id="<?php echo $array['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart-o"></i></a>
											<?php } else { ?>
											 <a title="Wishlisted" data-tooltip="Wishlisted" data-id="<?php echo $array['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart"></i></a>
													<?php } } else { ?>
												<a data-tooltip="Temporarily Unavailable" href="javascript:void(0)"><span style="font-size: 12px;">Temporarily Unavailable</span></a>
											<?php }} else { ?>
													<a data-tooltip="Login" href="auth.php"><i class="la la-user"></i></a>
											<?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-content product-content-padding">
                                        <h4><a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($array['service_id']))); ?>"><?php echo $array['service_name']; ?></a></h4>
                                        <div class="price-addtocart">
										<?php
										$discount = $array['discount_amount'];
										$price = $array['service_price'];
										$discount_type = $array['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
                                            <div class="product-price">
                                                <span>₹<?php echo $discount_price; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php } } ?>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
		
		<div class="product-area pb-40">
            <div class="container">
                <div class="section-title-tab-wrap">
                    <div class="row">
                        <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4">
                            <div class="section-title-5">
                                <h2>Our Studios</h2>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-8 col-md-8 col-sm-8">
                           
                        </div>
                    </div>
                </div>
                <div class="tab-content jump">
                    <div id="product-10" class="tab-pane active">
                        <div class="row">
						<?php
						$result = $con->query("SELECT * FROM vendors WHERE status = 1 LIMIT 4");
						if($result->num_rows == 0){
							echo '<h3 style="margin-left: 2%;">No Vendors Found!</h3>';
						}else{
							while($row = $result->fetch_assoc()){
						?>
						
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="product-wrap product-border-3 product-img-zoom mb-30">
                                    <div class="product-img">
                                        <a href="vendor-profile.php?vendor_id=<?php echo base64_encode(base64_encode(base64_encode($row['vendor_id']))); ?>"><img src="uploaded_files/bussinesslogo/<?php echo $row['bussinesslogo']; ?>" alt="Vendor"></a>
                                        <div class="product-action-4">
                                          
                                        </div>
                                    </div>
                                    <div class="product-content product-content-padding">
                                        <h4><a href="vendor-profile.php?vendor_id=<?php echo base64_encode(base64_encode(base64_encode($row['vendor_id']))); ?>"><?php echo $row['bussinessname']; ?></a></h4>
                                        <div class="price-addtocart">
                                            <div class="product-price">
                                                <a href="vendor-profile.php?vendor_id=<?php echo base64_encode(base64_encode(base64_encode($row['vendor_id']))); ?>" class="btn btn-info" style="font-size: 13px;">Explore</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
						<?php } } ?>
							
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
		
        
   <?php include 'includes/footer.php'; ?>