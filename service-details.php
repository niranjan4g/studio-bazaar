<?php include 'includes/header.php'; ?>
<?Php
if(!isset($_GET['service_id'])){
	$_SESSION['error'] = 'Please Select a Service to View!';
	header("location: shop.php");
	exit();
}else{
	$service_id = base64_decode(base64_decode(base64_decode($_GET['service_id'])));
	$result = $con->query("SELECT * FROM services WHERE service_id = '$service_id'");
	if($result->num_rows == 0){
		$_SESSION['error'] = 'No Service Found!';
		header("location: shop.php");
		exit();
	}else{
		$row = $result->fetch_assoc();
		$category_id = $row['category_id'];
		$record = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
		$vendor_id = $row['vendor_id'];
		$rec = $con->query("SELECT * FROM vendors WHERE vendor_id = '$vendor_id'")->fetch_assoc();
	}
}
?>
<body>
    <div class="main-wrapper">
         <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
         <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Service Details</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Service Details</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="product-details-area pt-90 pb-90">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="product-details-img-left">
                            <img  src="management/admin/uploaded_files/category/<?php echo $record['category_cover']; ?>"  alt="service-details-img">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="product-details-content pro-details-content-modify">
                            <span><?php echo $record['category_name']; ?></span>
                            <h2><?php echo $row['service_name']; ?></h2>
                            <div class="product-ratting-review">
                                <div class="product-ratting">
                                    Service By:
                                </div>
                                <div class="product-review">
                                    <span><?php echo $rec['bussinessname']; ?></span>
                                </div>
                            </div>
                            <div class="pro-details-color-wrap">
                                <span>Service Description</span>
                                <div class="pro-details-color-content">
                                   <?php echo $row['service_desc']; ?>
                                </div>
                            </div>
                            <?php
										$discount = $row['discount_amount'];
										$price = $row['service_price'];
										$discount_type = $row['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
                            <div class="pro-details-price-wrap">
                                <div class="product-price">
                                    <span>₹<?php echo number_format($discount_price, 2); ?></span>
									<?php if($discount_type == 1 || $discount_type == 2){ ?>
                                    <span class="old">₹<?php echo number_format($price, 2); ?></span>
									<?php } else { } ?>
                                </div>
								<?php if($discount_type == 1){ ?>
                                <div class="dec-rang"><span>- ₹<?php echo $discount; ?></span></div>
								<?php } else if($discount_type == 2) { ?>
								<div class="dec-rang"><span>- <?php echo $discount; ?>%</span></div>
								<?php } else { ?>
								
								<?php } ?>
                            </div>
                            <div class="product-ratting-review">
                                <div class="product-ratting">
                                    Advance Payment:
                                </div>
                                <div class="product-review">
                                    <span>10% of ₹<?php echo number_format($discount_price, 2); ?> = <b>₹<?php echo number_format((10/100)*$discount_price, 2); ?></b></span>
                                </div>
                            </div>
                            <div class="pro-details-compare-wishlist">
                              
                                <div class="pro-details-wishlist">
								<?php 
								if(isset($_SESSION['user'])){ 
								if($row['availability'] == 0) {
									$user_id = $_SESSION['user'];
									$service_id = $row['service_id'];
									$rer = $con->query("SELECT * FROM wishlist WHERE user_id = '$user_id' AND service_id = '$service_id'");
									if($rer->num_rows == 0) {
								?>
                                    <a title="Add To Wishlist" data-id="<?php echo $row['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart-o"></i> Add to wish list</a>
									<?php } else { ?>
									
									 <a title="Wishlisted" data-id="<?php echo $row['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart"></i> Wishlisted</a>
									<?php } } else { ?>
									 <a title="Temporarily Unavailable" style="color: red;"><i class="la la-cross"></i> Temporarily Unavailable. Stay Tuned!</a>
								<?php } } else { ?>
									 <a title="Login to Add to  Wishlist" href="auth.php"><i class="la la-user"></i>Login to Add to Wishlist</a>
								<?php } ?>
                                </div>
                            </div>
                            <div class="pro-details-buy-now btn-hover btn-hover-radious">
							<?php
							if(isset($_SESSION['user'])){
								if($row['availability'] == 0) {
									$user_id = $_SESSION['user'];
									$service_id = $row['service_id'];
									$resq = $con->query("SELECT * FROM cart WHERE user_id = '$user_id' AND service_id = '$service_id'");
									if($resq->num_rows == 0) {
							?>
                                <a href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">Add To Cart</a>
									<?php } else { ?>
								 <a href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">Already in Cart</a>
								<?php } } else { ?>
								
							<?php } }else{ ?>
								<a href="auth.php">Login to Add to Cart</a>
							<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="product-area pb-85">
            <div class="container">
                <div class="section-title-6 mb-50 text-center">
                    <h2>Similar Services</h2>
                </div>
               <div class="row">
				
				<?php
				$sql = "SELECT * FROM services WHERE is_active = 1 AND category_id = '$category_id' AND NOT service_id = '$service_id'  ORDER BY serviceid DESC";
				$res = $con->query($sql);
				if($res->num_rows == 0){
					echo "<h3>No Similar Services!</h3>";
				}else {
				while($rec = $res->fetch_assoc()) {
					$cat = $rec['category_id'];
					$ques = $con->query("SELECT * FROM categories WHERE category_id = '$cat'")->fetch_assoc();
				?>
				
				 <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="product-wrap">
                        <div class="product-img mb-15">
                            <a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($rec['service_id']))); ?> "><img src="management/admin/uploaded_files/category/<?php echo $ques['category_cover']; ?>" alt="Service"></a>
                            <div class="product-action">
                                <a title="View" href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($rec['service_id']))); ?>"><i class="la la-eye"></i></a>
								<?php if(isset($_SESSION['user'])) { 
									if($rec['availability'] == 0) {
									$user_id = $_SESSION['user'];
									$service_id = $rec['service_id'];
									$rerr = $con->query("SELECT * FROM wishlist WHERE user_id = '$user_id' AND service_id = '$service_id'");
									if($rerr->num_rows == 0) {
								?>
                                  <a title="Add To Wishlist" data-id="<?php echo $rec['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart-o"></i></a>
									<?php } else { ?>
									  <a title="Wishlisted" data-id="<?php echo $rec['service_id']; ?>" class="add-wishlist" href="javascript:void(0)"><i id="heart" class="la la-heart"></i></a>
									
									<?php } } else { ?>
								  <a title="Login" href="auth.php"><i class="la la-user"></i></a>
								<?php } } ?>
                            </div>
                        </div>
                        <div class="product-content">
                            <span><?php echo $ques['category_name']; ?></span>
                            <h4><a href="service-details.php?service_id=<?php echo base64_encode(base64_encode(base64_encode($rec['service_id']))); ?>"><?php echo $rec['service_name']; ?></a></h4>
                            <div class="price-addtocart">
                                <div class="product-price">
								    <?php
										$discount = $row['discount_amount'];
										$price = $row['service_price'];
										$discount_type = $row['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
                                    <span>₹<?php echo number_format($discount_price, 2); ?></span>
                                </div>
                                <div class="product-addtocart">
								<?php if(isset($_SESSION['user'])) { 
								if($rec['availability'] == 0) { 
								$user_id = $_SESSION['user'];
									$service_id = $rec['service_id'];
									$resp = $con->query("SELECT * FROM cart WHERE user_id = '$user_id' AND service_id = '$service_id'");
									if($resp->num_rows == 0) {
								?>
                                    <a title="Add To Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $rec['service_id']; ?>" data-vendor-id="<?php echo $rec['vendor_id']; ?>">+ Add To Cart</a>
									<?php } else { ?>
									<a title="Already in Cart" href="javascript:void(0)" class="add-cart" data-id="<?php echo $rec['service_id']; ?>" data-vendor-id="<?php echo $rec['vendor_id']; ?>">+ Added In Cart</a>
								<?php } } else { ?> 
									 <a title="Temporarily Unavailable">Unavailable</a>
								<?php } } else { ?>
									<a title="Login" href="auth.php">+ Login to Add to Cart</a>
								<?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
					
				<?php } } ?>
					
					
                </div>
            </div>
        </div>
		
		
		
 <?php include 'includes/footer.php'; ?>
 