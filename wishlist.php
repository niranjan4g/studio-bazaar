<?php include 'includes/header.php'; ?>
<?php
if(isset($_SESSION['user'])){
	$user_id = $_SESSION['user'];
	$sql = "SELECT * FROM wishlist WHERE user_id ='$user_id'";
	$result = $con->query($sql);
}else{
	$_SESSION['error'] = 'You are Not Authorized! Please Login';
	header("location: index.php");
	exit();
}
?>
<body>
    <div class="main-wrapper">
         <header class="header-area">
            <div class="main-header-wrap">
                <?php include 'includes/topbar.php'; ?>
                
				<?php include 'includes/brandbar.php'; ?>
				
                <?php include 'includes/desknav.php'; ?>
				
            </div>
           <?php include 'includes/mobile_head.php'; ?>
            
        </header>
        <?php include 'includes/mobile_nav.php'; ?>
		
		<?php
        if(isset($_SESSION['error'])){
          echo "
           <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'error',
				layout: 'topRight',
				text: '".$_SESSION['error']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['error']);
        }
        if(isset($_SESSION['success'])){
          echo "
            <script type='text/javascript'>
		   new Noty({
			    theme: 'sunset',
				type: 'success',
				layout: 'topRight',
				text: '".$_SESSION['success']."',
				timeout: 3000
			}).show();
		   </script>
          ";
          unset($_SESSION['success']);
        }
      ?>
        <div class="breadcrumb-area bg-img" style="background-image:url(assets/images/bg/breadcrumb.jpg);">
            <div class="container">
                <div class="breadcrumb-content text-center">
                    <h2>Wishlist</h2>
                    <ul>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li class="active">Wishlist </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="cart-main-area pt-85 pb-90">
            <div class="container">
                <h3 class="cart-page-title">Your Wishlisted Services</h3>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        
                            <div class="table-content table-responsive cart-table-content">
                                <table>
                                    <thead>
                                        <tr>
										 <th>#</th>
                                           
                                            <th>Service Name</th>
											<th>Category Name</th>
                                            <th>Service Price</th>
                                            <th>Date Added</th>
                                            <th>Remove</th>
                                            <th>Add To Cart</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									if($result->num_rows == 0) {
									?>
									<tr>
									<td colspan="7">No Services in your Wishlist!</td>
									</tr>
									<?php } else { 
									$count = 1;
									while($record = $result->fetch_assoc()) {
										$service_id = $record['service_id'];
										$row = $con->query("SELECT * FROM services WHERE service_id = '$service_id'")->fetch_assoc();
										$category_id = $row['category_id'];
										$array = $con->query("SELECT * FROM categories WHERE category_id = '$category_id'")->fetch_assoc();
									?>
                                        <tr>
										<td><?php echo $count++; ?></td>
                                          
                                            <td class="product-name"><?php echo $row['service_name'] ;?></td>
                                            <td class="product-price-cart"><?php echo $array['category_name']; ?></td>
                                            <td class="product-quantity">
											 <?php
										$discount = $row['discount_amount'];
										$price = $row['service_price'];
										$discount_type = $row['discount_type'];
										if($discount_type == 1){
											$discount_price = $price - $discount;
										}else if($discount_type == 2){
											$discount_price = $price - (($discount/100)*$price);
										}else{
											$discount_price = $price;
										}
										?>
										
                                              <span class="amount">₹<?php echo number_format($discount_price, 2); ?></span>
                                            </td>
                                            <td class="product-subtotal"><?php echo date('d F, Y', strtotime($record['created_on'])); ?></td>
											 <td>
                                                <a href="javascript:void(0)" data-id="<?php echo $record['wishlistid']; ?>" class="delete-wishlist"><i style="color: red; font-size: 22px;" class="la la-trash"></i></a>
                                            </td>
											<?php if($row['availability'] == 0){
												$user_id = $_SESSION['user'];
												$service_id = $row['service_id'];
												$vendor_id = $row['vendor_id'];
												$ren = $con->query("SELECT * FROM cart WHERE service_id = '$service_id' AND user_id = '$user_id'");
												if($ren->num_rows == 0){
												?>
                                            <td class="product-wishlist-cart">
                                                <a href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">Add to cart</a>
                                            </td>
												<?php } else { ?>
												  <td class="product-wishlist-cart">
                                                <a href="javascript:void(0)" class="add-cart" data-id="<?php echo $row['service_id']; ?>" data-vendor-id="<?php echo $row['vendor_id']; ?>">Already in cart</a>
                                            </td>
											<?php } } else { ?>
											  <td>
                                                <a style="color: red;">Temporarily Unavailable</a>
                                            </td>
											<?php } ?>
                                        </tr>
									<?php  } } ?>
										
                                    </tbody>
                                </table>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <?php include 'includes/footer.php'; ?>